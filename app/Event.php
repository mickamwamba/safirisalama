<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    //
    public function media(){
    	return $this->hasMany('App\EventMedia');
    }
}
