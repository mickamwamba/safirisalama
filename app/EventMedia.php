<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventMedia extends Model
{
    //
    protected $table = 'events_media';
    
    public function event(){
    	return $this->belongsTo('App\Event','event_id');
    }
}
