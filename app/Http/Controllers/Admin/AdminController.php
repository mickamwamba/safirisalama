<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Hash;
use App\User;

class AdminController extends Controller
{
    //
    public function authenticateAdmin(Request $request)
    {
        // echo 'adfasfsa'; exit();
        if (Auth::attempt(['phone' => $request->phone, 'password' => $request->password,'role_id'=>2])) {
            // Authentication passed...
            
            return redirect()->intended('/');

        }
        else return redirect()->back()->with('error','Credentials do not match our records');
    }

    public function logout()
    {
        Auth::logout();
        
        return redirect()->route('admin.login');
    }


    public function updateAdmin(Request $request){
        if ($admin = User::find($request->admin_id)) {
            $admin->fname = $request->fname;
            $admin->sname = $request->sname;
            // $admin->phone = $request->phone;
            if ($admin->save()) {
                return redirect()->route('admin.dashboard')->with('msg','Your details have been updated successfully');
            }

        }
        else redirect()->back()->with('msg','User not found');
    }


    public function updatePassword(Request $request){
        if ($admin = User::find($request->admin_id)) {
              
                 if (Hash::check($request->input('current_password'),$admin->password) && strlen($request->new_password)>=6) {
                        if ($request->new_password==$request->confirm_password) {
                        $admin->password = bcrypt($request->new_password);
                        if ($admin->save()) {
                            return redirect(url('/logout'));
                        }
                    } 
                }
        }
        return redirect()->back()->with('msg','Errors occured, Failed to change password');
    }

}
