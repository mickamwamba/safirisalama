<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Advocate;
use DB;
use PDF;

class AdvocateController extends Controller
{
    //
    public function getAdvocates()
    {
    	$count = 10;
    	if ($advocates = Advocate::orderBy('created_at','DESC')->paginate($count)) return view('admin.advocates',['advocates'=>$advocates]);
    	else return redirect()->back()->with('msg','Advocates not found'); 
    }
    public function verifyAdvocate(Request $request){
    	$selected = $request->input('advocates');
    	// var_dump($selected); exit();
    	if ($selected!=NULL) {
    		for ($i=0; $i < count($selected); $i++) { 
    			if ($selected[$i]=="undefined") {
    				continue;
    			}
    			$advocate_id = $selected[$i];
    			$advocate = Advocate::find($advocate_id);

    			$advocate->status = '1';
    			$advocate->save();
    		}
    		return redirect('/admin/advocates')->with('msg','Advocate verified');
    	}
    	else{
    		return redirect('/admin/advocates')->with('msg','No advocate to be verified');
    	}
    }

       public function pdfview(Request $request)
    {

       if ($advocate = Advocate::find(1)) 
       	{
       	view()->share('advocate',$advocate);

        if($request->has('download')){
            $pdf = PDF::loadView('admin.pdfview');
            return $pdf->download('pdfview.pdf');
        }

        return view('admin.pdfview');
    }
    else return redirect()->back()->with('msg','Advocate request not found');
        
  } 
    
}
