<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Libraries\FileManager;
use App\Http\Controllers\Controller;
use App\Asasi;

class AsasiController extends Controller
{
    //
    public function getAsasi(){
        $count = 10;
        $asasi = Asasi::paginate($count);
        // var_dump($asasi); exit();
        if(!$asasi) return redirect()->route('admin.asasi');
        return view('admin.asasi',['asasi'=>$asasi]);
    }

    public function showAddAsasi(){
    	return view('admin.addAsasi');
    }

    public function addAsasi(Request $request){

    	$asasi = new Asasi;
        $asasi->name = $request->name;
        $asasi->abbreviation = $request->abbreviation;    
    	$asasi->website = $request->website;                    
        $asasi->description = $request->description;

    	$asasi->logo  = FileManager::upload($request,'logo','/images/associations');
    	if ($asasi->save()) return redirect()->route('admin.asasi')->with('msg','Association added successfully');
    	else return redirect()->route('admin.asasi')->with('msg','Failed to add asscociation');
    }

    public function showEditAsasiPanel($asasi_id){
        if ($asasi = Asasi::find($asasi_id)) return view('admin.editAsasi',['asasi'=>$asasi]);
    	else return redirect()->back()->with('msg','Asasi not found');
    }

    public function editAsasi(Request $request){
        if ($asasi = Asasi::find($request->asasi_id)) {
    		$asasi->name = $request->name;
            $asasi->abbreviation = $request->abbreviation;
            $asasi->description = $request->description;
            $asasi->website = $request->website;
            
            
    		$asasi->logo = ($request->hasFile('logo'))?FileManager::upload($request,'logo','/images/news'):$asasi->logo;
 
    		if ($asasi->save()) return redirect()->route('admin.asasi')->with('msg','Asasi edited successfully');
    		else return redirect()->back()->with('msg','Failed to edit asasi');
    	}
    	else return redirect()->back()->with('msg','Asasi not found');
    }

    public function deleteAsasi($asasi_id)
    {
    	if ($asasi= Asasi::find($asasi_id)) {
    		if ($asasi->delete()) return redirect()->route('admin.asasi')->with('msg','asasi deleted successfully');
    		else return redirect()->back()->with('msg','Failed to delete asasi');
    	}
    }   
}