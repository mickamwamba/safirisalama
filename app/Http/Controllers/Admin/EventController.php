<?php

namespace App\Http\Controllers\Admin;
use App\Libraries\CustomDate;
use App\Libraries\FileManager;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Event;
use App\EventMedia;

use DB; 

class EventController extends Controller
{
    //
    public function getEvents()
    {
    	$evts = Event::with('media')->orderBy('created_at','DESC')->paginate(10);
    	return view('admin.events',['events'=>$evts]);
	}

	public function showAddPanel()
	{
		return view('admin.addEvent');
	}

	public function addEvent(Request $request)
		{
			$event = new Event();
			$event->title = $request->title;
			$event->start_date =CustomDate::createMysqlDate($request->start_date);
			$event->end_date = CustomDate::createMysqlDate($request->end_date);
			$event->description = $request->content;
			$event->venue = $request->venue;
			try 
			{
				DB::transaction(function() use ($request,$event){
				$event->save();
				$media = new EventMedia;
				$media->media_type = 'IMAGE';
				$media->media = FileManager::upload($request,'image','/images/events');
				$media->event_id = $event->id;
				$media->save();
				});

			} 

			catch (Exception $e) {
				return redirect('admin/events')->with('msg','Failed to add event');
			}

			return redirect('admin/events')->with('msg','Event added successfully');
	}

	public function showEditPanel($event_id){
		if ($event = Event::find($event_id)) return view('admin.editEvent',['event'=>$event]);
		else return redirect()->back()->with('msg','Event not found');
	}


	public function editEvent(Request $request)
	{
		if($event = Event::find($request->event_id))

			{
				$event->title = $request->title;
				$event->start_date =CustomDate::createMysqlDate($request->start_date);
				$event->end_date = CustomDate::createMysqlDate($request->end_date);
				$event->description = $request->description;
				$event->venue = $request->venue;

		        if ($event->save()) return redirect('admin/events')->with('msg','Event edited successfully');
   				else return redirect()->back()->with('msg','Failed to edit event');
   			}
   			else return redirect()->back()->with('msg','Event not found');

	}

	public function deleteEvent($event_id)
	{
		if($event = Event::with('media')->find($event_id)){
			foreach ($event->media as $md)
			{
				$md->delete();
			}

			if($event->delete()) return redirect()->back()->with('msg','Event deleted successfully');
			else return redirect()->back()->with('msg','Failed to delete event');
		}
		else return redirect()->back()->with('msg','Event not found');
	}

}