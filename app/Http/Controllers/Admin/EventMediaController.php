<?php

namespace App\Http\Controllers\Admin;

use App\Libraries\FileManager;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\EventMedia;

class EventMediaController extends Controller
{
    //
    public function getMedia($event_id){
    if ($media = EventMedia::with('event')->where('event_id',$event_id)->get())  return view('admin.manageEventMedia',['eventMedia'=>$media,'event_id'=>$event_id]);
   
    else redirect()->back()->with('msg','No media found');
    }

    public function addMedia(Request $request){
    	$media = new EventMedia;
    	$media->media_type = $request->media_type;
    	$media->event_id = $request->event_id;
    	$media->media = FileManager::upload($request,'media','/images/events');

    	if ($media->save()) return redirect()->back()->with('msg','Media uploaded successfully');
    	else return redirect()->back()->with('msg','Failed to upload media');
    }

    public function showEditPanel($event_media_id)
    {
    	if ($event_media = EventMedia::find($event_media_id)) return view('admin.editEventMedia',['event_media'=>$event_media]);
    	else return redirect()->back()->with('msg','Media not found');
    }	

    public function editEventMedia(Request $request)
    {
    	if ($media = EventMedia::find($request->event_media_id)) {
    		$media->media_type = $request->media_type;
    		$media->media = ($request->hasFile('media'))?FileManager::upload($request,'media','/images/events'):$media->media;
    		if($media->save()) return redirect('/admin/eventMedia/'.$media->event_id)->with('msg','Media edited successfully');
    		else return redirect()->back()->with('msg','Failed to edit event media');
    	}
    	else return redirect()->back()->with('msg','Event media not found');
    }

    public function deleteEventMedia($event_media_id)
    {
    	if ($media = EventMedia::find($event_media_id)) {
    		if($media->delete()) return  redirect()->back()->with('msg','Event media deleted successfully');
    		else  redirect()->back()->with('msg','Failed to delete event media');
    	}
    	else return redirect()->back()->with('msg','Event media not found');
    }
}
