<?php

namespace App\Http\Controllers\Admin;

use App\Libraries\FileManager;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\News;

class NewsController extends Controller
{

    public function getNews()
    {
    	$count = 10;
    	if ($news = News::orderBy('created_at','DESC')->paginate($count)) {
    		return view('admin.news',['news'=>$news]);
    	}
    	else return redirect()->back()->with('msg','News not found');
    }

    public function showAddPanel()
    {
    	return view('admin.addNews');
    }

    public function addNews(Request $request)
    {
    	$news = new News;
    	$news->title = $request->title;
    	$news->content = $request->content;
    	// $news->image  = FileManager::upload($request,'image','/images/news');
		$news->video = $request->video;
    	if ($news->save()) return redirect()->route('admin.allNews')->with('msg','News added successfully');
    	else return redirect()->route('admin.allNews')->with('msg','Failed to add news');
    }

    public function showEditPanel($news_id)
    {	
    	if ($news = News::find($news_id)) return view('admin.editNews',['news'=>$news]);
    	else return redirect()->back()->with('msg','News not found');
    }

    public function editNews(Request $request)
    {
    	if ($news = News::find($request->news_id)) {
    		$news->title = $request->title;
			$news->content = $request->content;
			$news->video = $request->video;
    		// $news->image = ($request->hasFile('image'))?FileManager::upload($request,'image','/images/news'):$news->image;
 
    		if ($news->save()) return redirect()->route('admin.allNews')->with('msg','News edited successfully');
    		else return redirect()->back()->with('msg','Failed to edit news');
    	}
    	else return redirect()->back()->with('msg','News not found');
    }

    public function deleteNews($news_id)
    {
    	if ($news = News::find($news_id)) {
    		if ($news->delete()) return redirect()->route('admin.allNews')->with('msg','News deleted successfully');
    		else return redirect()->back()->with('msg','Failed to delete news');
    	}
    }

    public function uploadFile($request,$file_name,$destinationPath)
    {
        $file = substr($request->file($file_name)->getClientOriginalName(),0,-4);
        $fileName = $file.time().".".$request->file($file_name)->getClientOriginalExtension();
        $request->file($file_name)->move(public_path().$destinationPath, $fileName);
        return $destinationPath."/".$fileName;
    }
}
