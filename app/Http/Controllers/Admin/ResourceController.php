<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Libraries\FileManager;
use App\Resource;

class ResourceController extends Controller
{
    //

    public function getResources(){
        $count = 10;
    	if ($resources = Resource::orderBy('created_at','DESC')->paginate($count)) {
    		return view('admin.resources',['resources'=>$resources]);
    	}
    	else return redirect()->back()->with('msg','News not found');
    }

    public function addRipoti(Request $request){

    	$resource = new Resource;
		$resource->title = $request->title;
		$resource->description = $request->description;
		$resource->type = $request->type;    
    	$resource->path  = FileManager::upload($request,'document','/resources');
    	if ($resource->save()) return redirect()->route('admin.resources')->with('msg','Resource added successfully');
    	else return redirect()->route('admin.resources')->with('msg','Failed to add resource');
    }

	public function showEditRipotiPanel($resource_id){
		if($resource = Resource::find($resource_id)){
			return view('admin.editResource',['resource'=>$resource]);
		}
		else return redirect()->back()->with('msg','Ripoti not found');
	}
    public function editRipoti(Request $request){
        if ($resource = Resource::find($request->resource_id)) {
    		$resource->title = $request->title;
			$resource->type = $request->type;
			$resource->description = $request->description;
    		$resource->path = ($request->hasFile('document'))?FileManager::upload($request,'document','/resources'):$resource->path;
 
    		if ($resource->save()) return redirect()->route('admin.resources')->with('msg','Resource edited successfully');
    		else return redirect()->back()->with('msg','Failed to edit resource');
    	}
    	else return redirect()->back()->with('msg','Resource not found');
    
    }

    public function deleteResource($resource_id){
        if ($resource= Resource::find($resource_id)) {
    		if ($resource->delete()) return redirect()->route('admin.resources')->with('msg','resource deleted successfully');
    		else return redirect()->back()->with('msg','Failed to delete resource');
    	}
    }

}
