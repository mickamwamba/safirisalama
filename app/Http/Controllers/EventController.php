<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Event;

class EventController extends Controller
{
    //

    public function getEvents()
    {
        if ($events = Event::with('media')->orderBy('created_at','DESC')->paginate(10)) {
           return view('events',['events'=>$events]);
        }
        else return redirect()->back();
    }

    public function getEvent($event_id)
    {
        if ($event = Event::with('media')->find($event_id)) 
        {
           return view('event',['event'=>$event]);
        }
        else return redirect()->back();
    }
}

