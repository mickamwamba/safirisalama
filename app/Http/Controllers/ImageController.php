<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;

class ImageController extends Controller
{
  
       public function createImage(Request $request)
       {
       
        //PHP's GD class functions can create a variety of output image 
        //types, this example creates a jpeg 
        // header("Content-Type: image/png"); 
        
        // //open up the image you want to put text over 
        // $url = public_path()."/images/white.png";
        // $font = public_path()."/fonts/Verdana.ttf";
        // // var_dump($url); exit();
        // $im = imagecreatefrompng($url);  
    
        // //The numbers are the RGB values of the color you want to use 
        // $black = ImageColorAllocate($im, 213, 33, 71); 
        
        // //The canvas's (0,0) position is the upper left corner 
        // //So this is how far down and to the right the text should start 
        // $start_x = 50; 
        // $start_y = 60; 
        
        // //This writes your text on the image in 50 point using verdana.ttf 
        // //For the type of effects you quoted, you'll want to use a truetype font 
        // //And not one of GD's built in fonts. Just upload the ttf file from your 
        // //c: windows fonts directory to your web server to use it. 
        // Imagettftext($im, 10, 0, $start_x, $start_y, $black,$font, " Mimi ".$request->name.",\n nasema \n #Ajalisasabasi"); 
        
        // //Creates the jpeg image and sends it to the browser 
        // //100 is the jpeg quality percentage 
        // $imagePath =  "/images/take_action/". str_replace(' ', '', $request->name).time().".png";
        // Imagepng($im,public_path().$imagePath,9); 
        
        // ImageDestroy($im); 
        return view('share_image',['name'=>$request->name]);
       }

       public function shareToFB(Request $request)
       {
            // var_dump($request->image); exit();

            $fb = new \Facebook\Facebook([
                'app_id' => '145522809563795',
                'app_secret' => 'c334f212236db64784781f746a6a8ad0',
                'default_graph_version' => 'v2.2',
                ]);
              
              $data = [
                'message' => '....',
                'source' => $fb->fileToUpload(public_path().$request->image),
              ];
              
              try {
                // Returns a `Facebook\FacebookResponse` object
                //$response = $fb->post('/me/photos', $data, $request->token);
                Session::flash('msg', "Thank you for showing support to the campaign #Ajalisasabasi");
                return redirect('/take_action');
              } catch(Facebook\Exceptions\FacebookResponseException $e) {
                echo 'Graph returned an error: ' . $e->getMessage();
                exit;
              } catch(Facebook\Exceptions\FacebookSDKException $e) {
                echo 'Facebook SDK returned an error: ' . $e->getMessage();
                exit;
              }
              
              $graphNode = $response->getGraphNode();
              
              //echo 'Photo ID: ' . $graphNode['id'];
              Session::flash('msg', "Thank you for showing support to the campaign #Ajalisasabasi");
              return redirect('/take_action');
        }
}
