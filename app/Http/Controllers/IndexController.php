<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\News;
use App\Event;

class IndexController extends Controller
{
    //
    public function getIndex()
    {
        $news = News::orderBy('created_at','DESC')->paginate(3);
        $events = Event::orderBy('created_at','DESC')->paginate(3);
        return view('index',['news'=>$news,'events'=>$events]);
        
    }
}
