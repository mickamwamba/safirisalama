<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\News;

class NewsController extends Controller
{
    //
    public function getAllNews()
    {
        if ($news = News::orderBy('created_at','DESC')->paginate(10)) {
            return view('allNews',['news'=>$news]);
        }
        else return redirect()->back();
    }
    public function getNews($news_id)
    {
        if ($news = News::find($news_id)) {
            $news_list = News::orderBy('created_at','DESC')->get();
            return view('news',['news'=>$news,'news_list'=>$news_list]);
        }
        else return redirect()->back();
    }
}
