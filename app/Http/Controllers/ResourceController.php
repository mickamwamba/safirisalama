<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Resource;

class ResourceController extends Controller
{
    //

    public function getResources(){
        $resources = Resource::orderBy('created_at','desc')->paginate(10);
        if(!$resources) return redirect()->back();
        return view('resources',['resources'=>$resources]);
    }
}
