<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
class CreateProposedChangesTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'proposed_changes';
    /**
     * Run the migrations.
     * @table proposed_changes
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->text('change')->nullable();
            $table->unsignedInteger('law_id');
            $table->timestamps();

            $table->index(["law_id"], 'fk_proposed_changes_laws1_idx');


            $table->foreign('law_id', 'fk_proposed_changes_laws1_idx')
                ->references('id')->on('laws')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
