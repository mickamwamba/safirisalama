@extends('layouts.app')

@section('content')
<div class="container">
  <div class="page-header">
    <h3>Add event</h3>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-md-8 col-md-offset-2">
        <form class="form-hor0izontal inner-form" method="POST" enctype="multipart/form-data" action="/admin/addEvent">
          <fieldset>
            <div class='row'>

            <div class="form-group form-elements form-group-custom label-floating co-md-12">
              <label for="title" class="control-label">Title</label>
              <input type="text" class="form-control form-custom" id="Title" name="title" value="" required>
            </div>

            <div class="form-group form-elements form-group-custom label-floating col-md-6">
              <label for="startdate" class="control-label">Start date</label>
              <input type="text" class="form-control form-custom" id="date-start" name="start_date" required>
            </div>

            <div class="form-group form-elements form-group-custom label-floating col-md-6">
              <label for="inputEmail" class="control-label">End date</label>
              <input type="text" class="form-control form-custom" id="date-end" name="end_date" required>
            </div>


            <div class="form-group form-elements form-group-custom label-floating col-md-6">
              <label for="inputEmail" class="control-label">Venue</label>
              <input type="text" class="form-control form-custom" id="venue" name="venue" value="" required>
            </div>

            <div class="form-group form-elements form-group-custom label-floating col-md-12">
               <div class="form-group" id="newspanel"><textarea  rows="15" cols="70" name="content" id="content-add-ta" required> </textarea></div><br />
            </div>
        
            <div class="form-group is-empty is-fileinput col-md-12">
              <!--<label class="control-label" for="inputFile3">File</label>-->
              <input type="file" id="inputFile3" name="image" multiple="" required>
              <input type="text" readonly="" class="form-control" placeholder="Upload file here" required>
            </div>
               <div class="row seperator">
                
              </div>
               {{ csrf_field() }}

              <div class="form-group btn-wrapper">
                <div class="col-md-12">
                  <button type="submit" class="btn btn-raised btn-success btn-post" name="submit" value="submit">post</button>
                </div>
              </div>
            </div>
        </form>
        @endsection

         <script type="text/javascript" src="{{URL::asset('tinymce/js/tinymce/tinymce.min.js')}}"></script>
        <script type="text/javascript">
            tinymce.init({
                selector: "#content-add-ta",
                plugins: [
                    "advlist autolink lists link image charmap print preview anchor",
                    "searchreplace visualblocks code fullscreen",
                    "insertdatetime media table contextmenu paste"
                ]
            });
        </script>