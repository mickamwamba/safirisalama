@extends('layouts.app') 

@section('content')
<div class="container">
  <div class="page-header">
    <h3>Add News</h3>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-md-8 col-md-offset-2">
        <form class="form-hor0izontal inner-form" method="POST" enctype="multipart/form-data" action="{{route('admin.addNews')}}">
          <fieldset>
            <div class='row'>
            <div class="form-group form-elements form-group-custom label-floating co-md-12">
              <label for="inputEmail" class="control-label">Title</label>
              <input type="text" class="form-control form-custom" id="Title" name="title" value="" required>
            </div>

            <div class="form-group form-elements form-group-custom label-floating co-md-12">
              <label for="inputEmail" class="control-label">Video Link</label>
              <input type="text" class="form-control form-custom" id="video" name="video" value="" required>
            </div>
         
              <div class="form-group" id="newspanel"><textarea  rows="15" cols="70" name="content" id="content-add-ta" required> </textarea></div><br />



              {{-- <div class="form-group is-empty is-fileinput col-md-12">
              <!--<label class="control-label" for="inputFile3">File</label>-->
              <input type="file" id="inputFile3" name="image" multiple="" required>
              <input type="text" readonly="" class="form-control" placeholder="Upload image here">
            </div> --}}
               <div class="row seperator">
                
              </div>
                {{ csrf_field() }}
              <div class="form-group btn-wrapper">
                <div class="col-md-12">
                  <button type="submit" class="btn btn-raised btn-success btn-post" name="submit" value="submit">post</button>
                </div>
              </div>
            </div>
        </form>
        @endsection

        <script type="text/javascript" src="{{URL::asset('tinymce/js/tinymce/tinymce.min.js')}}"></script>
        <script type="text/javascript">
            tinymce.init({
                selector: "#content-add-ta",
                plugins: [
                    "advlist autolink lists link image charmap print preview anchor",
                    "searchreplace visualblocks code fullscreen",
                    "insertdatetime media table contextmenu paste"
                ]
            });
        </script>