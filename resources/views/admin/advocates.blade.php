@extends('layouts.app')

@section('content')
<div class="subnavbar">
<ul  class="nav nav-pills">
      <li class="active">
        <a  href="#1a" data-toggle="tab">ALL ADVOCATES REQUESTS</a>
      </li>
      <li><a href="#2a" data-toggle="tab">NEW REQUESTS</a>
      </li>
    </ul>
    </div>    
<!-- <div class="container"><h3>Markets - Sell Enquiries </h3></div> -->
<div id="exTab1" class="container"> 

      <div class="tab-content clearfix">
        <div class="tab-pane active" id="1a">
           <div class="row">
        <div class="col-md-12">
        <?php
          $count = 0;
          for($i=0; $i<count($advocates); $i++){
            if($advocates[$i]->status==0)
            $count++;        
          }
        ?>
        
        <table class="table table-striped table-hover animated fadeIn">
  <thead>
  <tr>
    <th>#</th>
    <th>First name</th>
    <th>Surname</th>
    <th>Phone</th>
     <th>Location</th>
    <th>Date</th>
   
  </tr>
  </thead>
  <tbody>
  <?php 
    $i = 1;
  ?>
  @foreach($advocates as $advocate)
  <tr>
  <th>{{$i++}}</th>
    <th>{{$advocate->fname}}</th>
    <th>{{$advocate->sname}}</th>
    <th>{{$advocate->phone}}</th>
     <th>{{$advocate->location}}</th>
    <th>{{date("D,d M,y",strtotime($advocate->created_at))}}</th>
    <td><a href="/admin/pdfview">  <i class="fa fa-paper-plane" aria-hidden="true"></i></span></a></td>
    </tr>
  @endforeach
  
  </tbody>
</table>

        </div>
        <div class="row">
          <nav>
           <ul class="pager">
     	     {{$advocates->links()}}
            </ul>
          </nav>
        </div>
    </div>
        </div>
        <div class="tab-pane" id="2a">
          <div class="tab-content clearfix">
        <div class="tab-pane active" id="1a">
           <div class="row">
        <div class="col-md-12">
  @if($count>0)
  <table class="table table-striped table-hover animated fadeIn">
  <thead>
  <tr>
    <th>#</th>
    <th>First name</th>
    <th>Surname</th>
    <th>Phone</th>
    <th>Location</th>
    <th>Date</th>
    <th></th>
   
     <th><div class="form-group select-all">
      <div class="checkbox checkbox-wrapper">
      <label>
      <input type="checkbox" id="select_all" onclick="selectAll()">
      </label>
      <span>Select all</span>
      </div>
      </div></th>
  </tr>
  </thead>
  <tbody>
  <?php 
    $i = 1;
  ?>
  @foreach($advocates as $advocate)
  @if($advocate->status==0)
  <tr>
  <th>{{$i++}}</th>
    <th>{{$advocate->fname}}</th>
    <th>{{$advocate->sname}}</th>
    <th>{{$advocate->phone}}</th>
    <th>{{$advocate->location}}</th>
    <th>{{date("D,d M,y",strtotime($advocate->created_at))}}</th>
    <td><a href="/admin/pdfview">  <i class="fa fa-paper-plane" aria-hidden="true"></i></span></a></td>

    <td><div class="form-group">
      <div class="checkbox">
      <label>
      <input type="checkbox" name="action" value="{{$advocate->id}}">
      </label>
      </div>
      </div>
      </td>
      </tr>
  @endif
  @endforeach
  
  </tbody>
</table>
<div class="submit_button">
  <a href="#" class="btn btn-raised btn-success pull-right" onclick="verifyAdvocate()">Verify</a>
</div>
@else
<div style="text-align: center;border: 1px solid #d9d4d4;color: #5e1d1d;"><h2>No new requests</h2></div>
@endif

      </div>
    </div>
        </div> 
        </div>
      </div>
  </div>
<!-- Bootstrap core JavaScript
    ================================================== -->
  <!-- Placed at the end of the document so the pages load faster -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
  <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
  <script type="text/javascript">
    function selectAll(source){
      var checkbox;
      var select_all = document.getElementById('select_all');

      checkboxes = document.getElementsByName('action');
        for (var i = 0; i < checkboxes.length; i++) {
          if (select_all.checked==true) {
            checkboxes[i].checked = true;
          }
          else{
            checkboxes[i].checked = false;
          }
      }
    }

    function verifyAdvocate(){
      var method = "POST";
      var path = "/admin/verifyAdvocate";
      var selected = new Array();
      checkboxes = document.getElementsByName('action');
      for (var i = 0; i < checkboxes.length; i++) {
        if (checkboxes[i].checked) {
          // selected.push(checkboxes[i].value);
            selected[i] = checkboxes[i].value;
        }
      }
      var hiddenField;
      var form = document.createElement("form");
       form.setAttribute("method", method);
       form.setAttribute("action", path);

       for (var i = 0; i < selected.length; i++) {
         hiddenField = document.createElement("input");
         hiddenField.setAttribute("type", "hidden");
         hiddenField.setAttribute("name", 'advocates[]');  
         hiddenField.setAttribute("value", selected[i]);
         form.appendChild(hiddenField);
       }
       document.body.appendChild(form);
       form.submit();
    }

    // function deletePost(post_id){
    // var result;
    // result = confirm("Are you sure you want to delete this post?");
    // if (result) {
    //   window.location.href = "/admin/deletePost/"+post_id;
    // }
  
    // }

  </script>
@endsection
