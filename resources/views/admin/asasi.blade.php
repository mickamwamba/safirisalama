@extends('layouts.app')

@section('content')
    <div class="container">
      <div class="page-header">
        <h3>Asasi za kiraia</h3>
      </div>
<div class="container">
    <div class="row">
        <div class="col-md-12">
                 <table class="table table-striped table-hover animated fadeIn">
  <thead>
<tr>
    <th>#</th>
    <th>Name</th>
    <th>Logo</th>
     <th>Description</th>
     <th></th>
     <th></th>
  </tr>
  </thead>
  <tbody>
<?php 
  $i = 1;
?>
  @foreach($asasi as $asasi_item)

     <tr>
  <th>{{$i++}}</th>
    <th>{{$asasi_item->name.'('.$asasi_item->abbreviation.')'}}</th>
    
    <th><img class="img-circle img-resize" src="{{$asasi_item->logo}}" alt=""></th>
    <td><textarea style="margin: 0px;height: 150px;width: 350px; border: 1px solid #eeeeee;">{{strip_tags($asasi_item->description)}}</textarea></td> 


    <td><a href="edit_asasi/{{$asasi_item->id}}">  <i class="fa fa-pencil" aria-hidden="true"></i></span></a></td>
    <td onclick="deleteAsasi({{$asasi_item->id}})"><a href="#">  <i class="fa fa-trash-o delete-btn" aria-hidden="true"></i></span></a></td>
     </tr>
  @endforeach

 
  </tbody>
</table>
        </div>
        {{$asasi->render()}}
    </div>
</div>
<a href="{{url('addAsasi')}}" class="btn btn-success btn-fab add-fab-btn">
<i class="material-icons">add</i>
</a>
<div class="row">
          <nav>
           <ul class="pager">
          <?php echo $asasi->render(); ?>
            </ul>
          </nav>
        </div>

@endsection
<script type="text/javascript">
  function deleteAsasi(asasi_id){
    var result;
    result = confirm("Are you sure you want to delete this asasi?");
    if (result) {
      window.location.href = "delete_asasi/"+asasi_id;
    }
  }
</script>