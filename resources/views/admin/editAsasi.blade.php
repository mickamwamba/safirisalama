@extends('layouts.app') 

@section('content')
<div class="container">
  <div class="page-header">
    <h3>Edit Asasi</h3>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-md-8 col-md-offset-2">
        <form class="form-hor0izontal inner-form" method="POST" enctype="multipart/form-data" action="{{route('admin.editAsasi')}}">
          <fieldset>
            <div class='row'>
            <div class="form-group form-elements form-group-custom label-floating co-md-12">
              <label for="inputEmail" class="control-label">Name</label>
            <input type="text" class="form-control form-custom" id="Title" name="name" value="{{$asasi->name}}" required>
            </div>
            <div class="form-group form-elements form-group-custom label-floating co-md-12">
                <label for="inputEmail" class="control-label">Abbreviation</label>
            <input type="text" class="form-control form-custom" id="Title" name="abbreviation" value="{{$asasi->abbreviation}}" required>
            </div>

            <div class="form-group form-elements form-group-custom label-floating co-md-12">
                <label for="inputEmail" class="control-label">Website</label>
            <input type="text" class="form-control form-custom" id="Title" name="website" value="{{$asasi->website}}">
            </div>

              <div class="form-group" id="newspanel"><textarea  rows="15" cols="70" name="description" id="content-add-ta" required> {{$asasi->description}} </textarea></div><br />


              <div class="form-group is-empty is-fileinput col-md-12">
              <!--<label class="control-label" for="inputFile3">File</label>-->
              <input type="file" id="inputFile3" name="logo" multiple="">
              <input type="text" readonly="" class="form-control" placeholder="Upload Logo here">
            </div>
               <div class="row seperator">
                
              </div>
            <input type="hidden" name="asasi_id" value="{{$asasi->id}}">
                {{ csrf_field() }}
              <div class="form-group btn-wrapper">
                <div class="col-md-12">
                  <button type="submit" class="btn btn-raised btn-success btn-post" name="submit" value="submit">SUBMIT</button>
                </div>
              </div>
            </div>
        </form>
        @endsection

        <script type="text/javascript" src="{{URL::asset('tinymce/js/tinymce/tinymce.min.js')}}"></script>
        <script type="text/javascript">
            tinymce.init({
                selector: "#content-add-ta",
                plugins: [
                    "advlist autolink lists link image charmap print preview anchor",
                    "searchreplace visualblocks code fullscreen",
                    "insertdatetime media table contextmenu paste"
                ]
            });
        </script>