@extends('layouts.app')

@section('content')
<div class="container">
  <div class="page-header">
    <h3>Edit Media</h3>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-md-8 col-md-offset-2">
           <form method="POST" action="/admin/editEventMedia" enctype="multipart/form-data"> 
             <div class="form-group form-elements form-group-custom label-floating co-md-12">
                <label for="inputEmail" class="control-label">Media Type</label>
                <select class="form-control" name="media_type">
                 <option>--Select Media Type--</option>
                  <option value="IMAGE" {{$event_media->media_type == 'IMAGE'?'selected="selected"':''}}>IMAGE</option>
                  <option value="VIDEO" {{$event_media->media_type == 'VIDEO'?'selected="selected"':''}}>VIDEO</option>
                </select>
             </div>

             <div class="form-group is-empty is-fileinput col-md-12">
              <!--<label class="control-label" for="inputFile3">File</label>-->
              <input type="file" id="inputFile3" name="media" multiple="">
              <input type="text" readonly="" class="form-control" placeholder="Upload file here">
            </div>

          <input type="hidden" name="event_media_id" value="{{$event_media->id}}">
        {{ csrf_field() }}
        <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save</button>
      </div>
        </form>
        @endsection
         