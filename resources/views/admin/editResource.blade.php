@extends('layouts.app') 

@section('content')
<div class="container">
  <div class="page-header">
    <h3>Edit Resource</h3>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-md-8 col-md-offset-2">
        <form class="form-hor0izontal inner-form" method="POST" enctype="multipart/form-data" action="{{route('admin.editRipoti')}}">
            <fieldset>
              <div class='row'>
              <div class="form-group form-elements form-group-custom co-md-12">
                <label for="inputEmail" class="control-label">Title</label>
              <input type="text" class="form-control form-custom" id="resource_title_edit" name="title" value="{{$resource->title}}" required>
              </div>

            <div class="form-group" id="newspanel"><textarea  rows="15" cols="70" name="description" class="edit_description" id="contentEdit" required>{{$resource->description}}</textarea></div><br />
              

              <div class="form-group form-elements form-group-custom label-floating co-md-12">
                  <label for="inputEmail" class="control-label">Document type</label>
                  <select class="form-control" name="type" id="resource_type_edit">
                  <option value="PDF" {{$resource->type=='PDF'?'selected':''}}>PDF</option>
                  <option value="WORD" {{$resource->type=='WORD'?'selected':''}}>WORD</option>
                  </select>
              </div>

                <div class="form-group is-empty is-fileinput col-md-12">
                <!--<label class="control-label" for="inputFile3">File</label>-->
                <input type="file" id="inputFile3" name="document" multiple="">
                <input type="text" readonly="" class="form-control" placeholder="Upload File here">
              </div>
                 <div class="row seperator">
                  
                </div>
                  {{ csrf_field() }}
                <div class="form-group btn-wrapper">
                  <div class="col-md-12">
                  <input type="hidden" name="resource_id" id="resource_id_edit" value="{{$resource->id}}">
                    {{-- <button type="submit" class="btn btn-raised btn-success btn-post" name="submit" value="submit">SUBMIT</button> --}}
                  </div>
                </div>

                <div class="form-group btn-wrapper">
                  <div class="col-md-12">
                    <button type="submit" class="btn btn-raised btn-success btn-post" name="submit" value="submit">SAVE CHANGES</button>
                  </div>
                </div>

                {{-- <button type="submit" class="btn btn-raised btn-success">Submit</button> --}}
              </div>
    </div>
 
</form>
    
        @endsection

        <script type="text/javascript" src="{{URL::asset('tinymce/js/tinymce/tinymce.min.js')}}"></script>
        <script type="text/javascript">
            tinymce.init({
                selector: "#contentEdit",
                plugins: [
                    "advlist autolink lists link image charmap print preview anchor",
                    "searchreplace visualblocks code fullscreen",
                    "insertdatetime media table contextmenu paste"
                ]
            });
          </script>