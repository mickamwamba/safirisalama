@extends('layouts.app')

@section('content')
    <div class="container">
      <div class="page-header">
        <h3>Events </h3>
      </div>
<div class="container">
    <div class="row">
        <div class="col-md-12">
                 <table class="table table-striped table-hover animated fadeIn">
  <thead>
<tr>
    <th>#</th>
    <th>Title</th>
    <th>Image</th>
     <th>Venue</th>
     <th>Date</th>
     <th>Media</th>
     <th></th>
     <th></th>
  </tr>
  </thead>
  <tbody>
<?php 
  $i = 1;
?>
  @foreach($events as $event)

     <tr>
  <th>{{$i++}}</th>
    <th>{{$event->title}}</th>
    
    <th><img class="img-circle img-resize" src="{{$event->media()->first()?$event->media()->first()->media:''}}" alt=""></th>
    <th>{{$event->venue}}</th>
    <th>{{date("D,d M,y",strtotime($event->start_date))}}</th>
    <td><a href="/admin/eventMedia/{{$event->id}}">  <i class="fa fa-camera-retro" aria-hidden="true"></i></span></a></td>
    <td><a href="/admin/editEvent/{{$event->id}}">  <i class="fa fa-pencil" aria-hidden="true"></i></span></a></td>
    <td onclick="deleteEvent({{$event->id}})"><a href="#">  <i class="fa fa-trash-o delete-btn" aria-hidden="true"></i></span></a></td>
     </tr>
  @endforeach

 
  </tbody>
</table>
        </div>
        {{$events->render()}}
    </div>
</div>
<a href="{{url('admin/addEvent')}}" class="btn btn-success btn-fab add-fab-btn">
<i class="material-icons">add</i>
</a>
<div class="row">
          <nav>
           <ul class="pager">
          <?php echo $events->render(); ?>
            </ul>
          </nav>
        </div>

@endsection
<script type="text/javascript">
  function deleteEvent(event_id){
    var result;
    result = confirm("Are you sure you want to delete this event?");
    if (result) {
      window.location.href = "/admin/deleteEvent/"+event_id;
    }
  }
</script>