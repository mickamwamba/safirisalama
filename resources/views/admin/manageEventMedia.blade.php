@extends('layouts.app')

@section('content')
<div class="container">
  <div class="page-header">
    <h3>Event media - {{(count($eventMedia) >0)?$eventMedia[0]->event->title:''}}</h3>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <table class="table table-striped table-hover animated fadeIn">
          <thead>
          <tr>
            <th>#</th>
            <th>Media type</th>
            <th>Media</th>
            <th>Uploaded on</th>
            <th></th>
            <th></th>
          </tr>
          </thead>
          <tbody>
            <?php 
              $i = 1;
            ?>
           @if(count($eventMedia) >0)

           @foreach($eventMedia as $media)
            <tr>
            <th>{{$i++}}</th>
            <th>{{$media->media_type}}</th>
            <th><img class="img-circle img-resize" src="{{$media->media}}" alt=""></th>
            <th>{{date("D,d M,y",strtotime($media->created_at))}}</th>
            <td><a href="/admin/editEventMedia/{{$media->id}}">  <i class="fa fa-pencil" aria-hidden="true"></i></span></a></td>
            <td onclick="deleteMedia({{$media->id}})"><a href="#">  <i class="fa fa-trash-o delete-btn" aria-hidden="true"></i></span></a></td>
            </tr>
          @endforeach

          @endif
          </tbody>
        </table>
      </div>
        <a href="#" data-toggle="modal" data-target="#addMedia"  class="btn btn-success btn-fab add-fab-btn">
          <i class="material-icons">add</i>
        </a>
    </div>
  </div>  
</div>

<!-- Modal -->
<div class="modal fade" id="addMedia" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">New event media</h4>
      </div>
      <div class="modal-body">
        <form method="POST" action="/admin/addMedia" enctype="multipart/form-data"> 
        
             <div class="form-group form-elements form-group-custom label-floating co-md-12">
                <label for="inputEmail" class="control-label">Media Type</label>
                <select class="form-control" name="media_type">
                 <option>--Select Media Type--</option>
                  <option value="IMAGE">IMAGE</option>
                  <option value="VIDEO">VIDEO</option>
                </select>
             </div>

             <div class="form-group is-empty is-fileinput col-md-12">
              <!--<label class="control-label" for="inputFile3">File</label>-->
              <input type="file" id="inputFile3" name="media" multiple="">
              <input type="text" readonly="" class="form-control" placeholder="Upload file here">
            </div>

          <input type="hidden" name="event_id" value="{{$event_id}}">
        {{ csrf_field() }}
        <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save</button>
      </div>
        </form>
      </div>
      
    </div>
  </div>
</div>

@endsection