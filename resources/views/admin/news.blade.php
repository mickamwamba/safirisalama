@extends('layouts.app')

@section('content')
    <div class="container">
      <div class="page-header">
        <h3>News</h3>
      </div>
<div class="container">
    <div class="row">
        <div class="col-md-12">
           <table class="table table-striped table-hover animated fadeIn">
  <thead>
  <tr>
    <th>#</th>
    <th>Title</th>
    <th>Image</th>
    <th>Content</th>
    <th>Date</th>
    <th></th>
    <th></th>
  </tr>
  </thead>
  <tbody>
  <?php 
    $i = 1;
  ?>
  @foreach($news as $n)
  <?php 
  $re = '/(h.*v=)(.{11})/';
  $str = $n->video;
  preg_match_all($re, $str, $matches, PREG_SET_ORDER, 0);
 
  $video_id = $str&&count($matches)>0!=NULL?$matches[0][2]:'';
  $thumbnail = 'https://img.youtube.com/vi/'.$video_id.'/0.jpg';

  ?>
  <tr>
  <td>{{$i++}}</td>
  <td><a href="{{$n->video}}">{{$n->title}}</a></td>
    <td><img class="img-circle img-resize" src="{{$thumbnail}}" alt=""></td>
    <td><textarea style="margin: 0px;height: 150px;width: 350px; border: 1px solid #eeeeee;">{{strip_tags($n->content)}}</textarea></td> 
    <td>{{date("D,d M,y",strtotime($n->created_at))}}</td>
    <td><a href="editNews/{{$n->id}}">  <i class="fa fa-pencil" aria-hidden="true"></i></span></a></td>
    <td onclick="deleteNews({{$n->id}})"><a href="#">  <i class="fa fa-trash-o delete-btn" aria-hidden="true"></i></span></a></td>
     </tr>
  @endforeach
  
  </tbody>
</table>

        </div>
           {{$news->render()}}
    </div>
</div>
<a href="addNews" class="btn btn-success btn-fab add-fab-btn">
<i class="material-icons">add</i>
</a>
@endsection

<script type="text/javascript">
  function deleteNews(news_id){
    var result;
    result = confirm("Are you sure you want to delete this article?");
    if (result) {
      window.location.href = "deleteNews/"+news_id
    }
}
</script>