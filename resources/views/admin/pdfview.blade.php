<style type="text/css">
	table td, table th{
		border:1px solid black;
	}
</style>
<div class="container">

	<br/>
	<a href="{{route('pdfview',['download'=>'pdf']) }}">Download PDF</a>

	<h2 style="text-align: center;"> Safiri Salama Advocate Request Form</h2>
	
	<table class="table stripped-table bordered-table">
		<thead>
		<th>First name</th>
		<th>Surname</th>
		<th>Reason </th>
		</thead>
		<tbody>
			<td>{{$advocate->fname}}</td>
			<td>{{$advocate->sname}}</td>
			<td>{{$advocate->reason}}</td>
		</tbody>

	</table>
</div>