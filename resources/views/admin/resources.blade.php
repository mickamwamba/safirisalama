@extends('layouts.app')

@section('content')
    <div class="container">
      <div class="page-header">
        <h3>Ripoti</h3>
      </div>
<div class="container">
    <div class="row">
        <div class="col-md-12">
                 <table class="table table-striped table-hover animated fadeIn">
  <thead>
<tr>
    <th>#</th>
    <th>title</th>
    <th>Description</th>
     <th></th>
     <th></th>
  </tr>
  </thead>
  <tbody>
<?php 
  $i = 1;
?>
  @foreach($resources as $resource)

     <tr>
  <td></td>
    <td><img class="img-circle img-resize doc-logo" style="border-radius:1px;height:50px;width:50px; margin-right:10px" src="{{$resource->type=='PDF'?'/images/icons/pdf.png':'/images/icons/word.png'}}" alt=""></td>
     <td><a href="{{$resource->path}}">{{$resource->title}}</a></td>
    
    <td style="width:40%"> <div><p>{!!$resource->description!!}</p></div></td>

     <td><a class="editResource" href="editResource/{{$resource->id}}">  <i class="fa fa-pencil" aria-hidden="true"></i></span></a></td>
    <td onclick="deleteResource({{$resource->id}})"><a href="#">  <i class="fa fa-trash-o delete-btn" aria-hidden="true"></i></span></a></td>
     </tr>
  @endforeach

 
  </tbody>
</table>
        </div>
        {{-- {{$resources->render()}} --}}
    </div>
</div>
<a href="#" class="btn btn-success btn-fab add-fab-btn" data-toggle="modal" data-target="#addResourceModal">
<i class="material-icons">add</i>
</a>
<div class="row">
          <nav>
           <ul class="pager">
          <?php echo $resources->render(); ?>
            </ul>
          </nav>
        </div>


<!--Add Modal -->
<div class="modal fade" id="addResourceModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Add new resource</h4>
        </div>
        <div class="modal-body">
            <form class="form-hor0izontal inner-form" method="POST" enctype="multipart/form-data" action="{{route('admin.addRipoti')}}">
                <fieldset>
                  <div class='row'>
                  <div class="form-group form-elements form-group-custom label-floating co-md-12">
                    <label for="inputEmail" class="control-label">Title</label>
                    <input type="text" class="form-control form-custom" id="Title" name="title" value="" required>
                  </div>

              <div class="form-group" id="newspanel"> <label for="inputEmail" class="control-label">Short Description</label><textarea  rows="15" cols="70" name="description" id="content-add-ta" required> </textarea></div><br />
                  

                  <div class="form-group form-elements form-group-custom label-floating co-md-12">
                      <label for="inputEmail" class="control-label">Document type</label>
                      <select class="form-control" name="type" id="">
                          <option value="PDF">PDF</option>
                          <option value="WORD">WORD</option>
                      </select>
                  </div>
    
                    <div class="form-group is-empty is-fileinput col-md-12">
                    <!--<label class="control-label" for="inputFile3">File</label>-->
                    <input type="file" id="inputFile3" name="document" multiple="" required>
                    <input type="text" readonly="" class="form-control" placeholder="Upload File here">
                  </div>
                     <div class="row seperator">
                      
                    </div>
                      {{ csrf_field() }}
                    <div class="form-group btn-wrapper">
                      <div class="col-md-12">
                        {{-- <button type="submit" class="btn btn-raised btn-success btn-post" name="submit" value="submit">SUBMIT</button> --}}
                      </div>
                    </div>
                  </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-raised btn-success">Submit</button>
          {{-- <button type="submit" class="btn btn-raised btn-success btn-post" name="submit" value="submit">SUBMIT</button> --}}

        </div>
    </form>
        
      </div>
    </div>
  </div>


  
@endsection
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>

<script type="text/javascript">
  function deleteResource(resource_id){
    var result;
    result = confirm("Are you sure you want to delete this asasi?");
    if (result) {
      window.location.href = "delete_resource/"+resource_id;
    }
  }
</script>

<script type="text/javascript" src="{{URL::asset('tinymce/js/tinymce/tinymce.min.js')}}"></script>
<script type="text/javascript">
    tinymce.init({
        selector: "#content-add-ta",
        plugins: [
            "advlist autolink lists link image charmap print preview anchor",
            "searchreplace visualblocks code fullscreen",
            "insertdatetime media table contextmenu paste"
        ]
    });
</script>
