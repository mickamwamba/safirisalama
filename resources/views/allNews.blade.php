@extends('layouts.client')
@section('content')
@component('shared.navbar')
@slot('navbarClass')
    navbar-solid inverted
@endslot
@slot('btnClass')
    action-btn
@endslot
@slot('buttonTheme')
    button-theme
@endslot
@endcomponent



{{--  news block  --}}
<div class="news-block">
    <h2 class="center-header safiri-text">
        News
    </h2>
    <div class="container">
        <div class="row">
        @foreach ($news as $n)
        <?php 
        $re = '/(h.*v=)(.{11})/';
        $str = $n->video;
        preg_match_all($re, $str, $matches, PREG_SET_ORDER, 0);
       
        $video_id = $str!=NULL&&count($matches)?$matches[0][2]:'';
        $thumbnail = 'https://img.youtube.com/vi/'.$video_id.'/0.jpg';
        ?>
        @component('shared.news')
            @slot('link')
            {{$n->id}}
            @endslot
            @slot('thumbnail')
            {{$thumbnail}}
            @endslot
            @slot('newsDate')
            {{date("d",strtotime($n->created_at))}}
            @endslot
            @slot('newsMonth')
            {{date("M",strtotime($n->created_at))}}
            @endslot
            @slot('newsTitle')
            {{--  <a href="news"> Ut rerum distinctio fuga cupiditate est.</a>  --}}
           {{$n->title}}
            @endslot
            @slot('newsAuthor')
            Admin
            @endslot
        @endcomponent
        @endforeach

        </div>
    </div>
</div>

@component('shared.footer')
@endcomponent

@endsection
