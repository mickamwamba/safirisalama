@extends ('layouts.client')
@section('title','Asasi za kiraia')
@section('content')
@component('shared.navbar')
@slot('navbarClass')
    navbar-safiri
@endslot
@slot('btnClass')
    action-btn-inverse
@endslot
@slot('buttonTheme')
    button-theme-inverse
@endslot
@endcomponent
    <div class="stats-main-block">
        <div class="container">
            <div class="row">
                <h1 class="center-takwimu-header">Asasi za kiraia</h1>
                <div class="col-xs-8 col-xs-offset-2">
                                    <h4 class="center-page-header">It takes a village—and a lot of strong voices—to make progress</h4>

                </div>
            </div>
        </div>
    </div>
    <div class="asasi-intro-block">
        <div class="container">
            <div class="row">
                <div class="col-md-12 asasi-about-wrapper">
                <h3 class="asasi-header-text">Kuhusu kampeni</h3>
                <p class="asasi-main-text">
                Kampeni ya umoja wa asasi za kiraia unaolenga  kuimarishwa kwa sera na sheria ya usalama barabarani.
                </p>
                </div>
                <div class="col-md-6 col-md-offset-3 asasi-video-wrapper">
                <div class="embed-responsive embed-responsive-16by9">
                 <iframe class="center-block" width="560" height="315" src="https://www.youtube.com/embed/u8KZWJCXd7I" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                 </div>
                </div>
            </div>
        </div>
    </div>
    <div class="asasi-list-block">
    <div class="container">        
        <div class="row">

   <div class="col-lg-12">
        <div class="col-sm-4 asasi-logo-wrapper">
        <img class="center-block" src="./images/associations/TAWLA.png" alt="" style="height:43px">
        </div>
        <div class="col-sm-8 asasi-main-description">
        <h3>Tanzania Women Lawyers Association (TAWLA) </h3>
        <p class="asas-description">Tanzania Women Lawyers Association (TAWLA) is an Association founded in 1989 and officially registered in 1990. The founding members comprised a professional group of women lawyers who felt the need for an organization that could promote an environment guaranteeing equal rights and access to all by focusing on vulnerable and marginalised groups especially women and children. The founding members also recognised the need for women lawyers to foster mutual support for each other in professional advancement and social responsibility.
</p>
   <figure class="svg-wrapper">
        <svg width="24px" height="24px" viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g id="Partner-Detail-(Sea-Change)" transform="translate(-216.000000, -1022.000000)" fill="#d82146">
            <g id="Org-Links" transform="translate(215.000000, 1008.000000)">
                <path d="M23.0833912,25.1110773 L19.6944935,28.4999751 C19.8056049,27.222194 19.6389378,25.9721907 19.1944922,24.7777431 L20.9722746,22.9721828 C22.3333893,21.6110681 22.3333893,19.3888401 20.9722746,18.0277254 C19.6111599,16.6666107 17.3889319,16.6666107 16.0278172,18.0277254 L12.2778074,21.7499574 C10.9166927,23.1388499 10.9166927,25.3333001 12.2778074,26.7221926 C12.8889201,27.3055275 12.8889201,28.2499744 12.2778074,28.8333093 C11.6944725,29.4166442 10.7500256,29.4166442 10.1666907,28.8333093 C7.6389063,26.277747 7.6389063,22.1666251 10.1666907,19.6388407 L13.8889227,15.9166088 C16.4444849,13.3610465 20.5556068,13.3610465 23.0833912,15.9166088 C25.6389535,18.4443932 25.6389535,22.5555151 23.0833912,25.1110773 Z M15.8333722,32.3610963 L12.0833624,36.1111062 C9.55557799,38.6388906 5.4444561,38.6388906 2.88889384,36.1111062 C0.361109436,33.5555439 0.361109436,29.444422 2.88889384,26.9166376 L6.30556947,23.499962 C6.19445807,24.7777431 6.36112517,26.0555242 6.80557078,27.2499718 L5.02778834,29.0277542 C3.66667366,30.3888689 3.66667366,32.611097 5.02778834,33.9722117 C6.38890302,35.3333263 8.61113107,35.3333263 9.97224575,33.9722117 L13.6944777,30.2499797 C15.0833703,28.888865 15.0833703,26.6666369 13.6944777,25.3055223 C13.1111429,24.7221874 13.1111429,23.7499626 13.6944777,23.1666278 C14.2778126,22.5832929 15.2500374,22.5832929 15.8333722,23.1666278 C18.3611566,25.72219 18.3611566,29.8333119 15.8333722,32.3610963 Z" id="link"></path>
            </g>
        </g>
    </g>
    <span class="asasi-website-link"><a href="http://www.tawla.or.tz/
">www.tawla.or.tz</a></span>
</svg>
        </figure>
        </div>
        </div>

           <div class="col-lg-12">
        <div class="col-sm-4 asasi-logo-wrapper">
        <img class="center-block" src="./images/associations/TAMWA.png" alt="" style="height:93px">
        </div>
        <div class="col-sm-8 asasi-main-description">
        <h3>Tanzania media women association </h3>
        <p class="asas-description">
            Tanzania Media Women Association (TAMWA) is a non-partisan, non-profit sharing professional media membership Association legally registered. Its vision is to see peaceful Tanzanian society which respects human rights from a gender perspective. The Association is looking for qualified candidates to fill the following position;
    

</p>
   <figure class="svg-wrapper">
        <svg width="24px" height="24px" viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g id="Partner-Detail-(Sea-Change)" transform="translate(-216.000000, -1022.000000)" fill="#d82146">
            <g id="Org-Links" transform="translate(215.000000, 1008.000000)">
                <path d="M23.0833912,25.1110773 L19.6944935,28.4999751 C19.8056049,27.222194 19.6389378,25.9721907 19.1944922,24.7777431 L20.9722746,22.9721828 C22.3333893,21.6110681 22.3333893,19.3888401 20.9722746,18.0277254 C19.6111599,16.6666107 17.3889319,16.6666107 16.0278172,18.0277254 L12.2778074,21.7499574 C10.9166927,23.1388499 10.9166927,25.3333001 12.2778074,26.7221926 C12.8889201,27.3055275 12.8889201,28.2499744 12.2778074,28.8333093 C11.6944725,29.4166442 10.7500256,29.4166442 10.1666907,28.8333093 C7.6389063,26.277747 7.6389063,22.1666251 10.1666907,19.6388407 L13.8889227,15.9166088 C16.4444849,13.3610465 20.5556068,13.3610465 23.0833912,15.9166088 C25.6389535,18.4443932 25.6389535,22.5555151 23.0833912,25.1110773 Z M15.8333722,32.3610963 L12.0833624,36.1111062 C9.55557799,38.6388906 5.4444561,38.6388906 2.88889384,36.1111062 C0.361109436,33.5555439 0.361109436,29.444422 2.88889384,26.9166376 L6.30556947,23.499962 C6.19445807,24.7777431 6.36112517,26.0555242 6.80557078,27.2499718 L5.02778834,29.0277542 C3.66667366,30.3888689 3.66667366,32.611097 5.02778834,33.9722117 C6.38890302,35.3333263 8.61113107,35.3333263 9.97224575,33.9722117 L13.6944777,30.2499797 C15.0833703,28.888865 15.0833703,26.6666369 13.6944777,25.3055223 C13.1111429,24.7221874 13.1111429,23.7499626 13.6944777,23.1666278 C14.2778126,22.5832929 15.2500374,22.5832929 15.8333722,23.1666278 C18.3611566,25.72219 18.3611566,29.8333119 15.8333722,32.3610963 Z" id="link"></path>
            </g>
        </g>
    </g>
    <span class="asasi-website-link"><a href="https://tamwa.org/tamwa/
">www.tamwa.org/tamwa/</a></span>
</svg>
        </figure>
        </div>
        </div>

 <div class="col-lg-12">
        <div class="col-sm-4 asasi-logo-wrapper">
        <img class="center-block" src="./images/associations/SHIVHEADER.jpg" alt="" style="height:43px">
        </div>
        <div class="col-sm-8 asasi-main-description">
        <h3>Shirikisho la vyawa vya watu wenye ulemavu</h3>
        <p class="asas-description">Federation of Disabled People’s Organizations  is a non governmental federation which brings together ten national Disabled People’s Organisations (DPOs) established by the DPOs in 1992.
</p>
   <figure class="svg-wrapper">
        <svg width="24px" height="24px" viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g id="Partner-Detail-(Sea-Change)" transform="translate(-216.000000, -1022.000000)" fill="#d82146">
            <g id="Org-Links" transform="translate(215.000000, 1008.000000)">
                <path d="M23.0833912,25.1110773 L19.6944935,28.4999751 C19.8056049,27.222194 19.6389378,25.9721907 19.1944922,24.7777431 L20.9722746,22.9721828 C22.3333893,21.6110681 22.3333893,19.3888401 20.9722746,18.0277254 C19.6111599,16.6666107 17.3889319,16.6666107 16.0278172,18.0277254 L12.2778074,21.7499574 C10.9166927,23.1388499 10.9166927,25.3333001 12.2778074,26.7221926 C12.8889201,27.3055275 12.8889201,28.2499744 12.2778074,28.8333093 C11.6944725,29.4166442 10.7500256,29.4166442 10.1666907,28.8333093 C7.6389063,26.277747 7.6389063,22.1666251 10.1666907,19.6388407 L13.8889227,15.9166088 C16.4444849,13.3610465 20.5556068,13.3610465 23.0833912,15.9166088 C25.6389535,18.4443932 25.6389535,22.5555151 23.0833912,25.1110773 Z M15.8333722,32.3610963 L12.0833624,36.1111062 C9.55557799,38.6388906 5.4444561,38.6388906 2.88889384,36.1111062 C0.361109436,33.5555439 0.361109436,29.444422 2.88889384,26.9166376 L6.30556947,23.499962 C6.19445807,24.7777431 6.36112517,26.0555242 6.80557078,27.2499718 L5.02778834,29.0277542 C3.66667366,30.3888689 3.66667366,32.611097 5.02778834,33.9722117 C6.38890302,35.3333263 8.61113107,35.3333263 9.97224575,33.9722117 L13.6944777,30.2499797 C15.0833703,28.888865 15.0833703,26.6666369 13.6944777,25.3055223 C13.1111429,24.7221874 13.1111429,23.7499626 13.6944777,23.1666278 C14.2778126,22.5832929 15.2500374,22.5832929 15.8333722,23.1666278 C18.3611566,25.72219 18.3611566,29.8333119 15.8333722,32.3610963 Z" id="link"></path>
            </g>
        </g>
    </g>
    <span class="asasi-website-link"><a href="http://shivyawata.or.tz/ 
">www.shivyawata.or.tz</a></span>
</svg>
        </figure>
        </div>
        </div>

           <div class="col-lg-12">
        <div class="col-sm-4 asasi-logo-wrapper">
        <img class="center-block" src="./images/associations/TLS.jpg" alt="" style="height:90px">
        </div>
        <div class="col-sm-8 asasi-main-description">
        <h3>Tanzania Lawyers Society </h3>
        <p class="asas-description">
Tanzania Women Lawyers Association (TAWLA) is an Association founded in 1989 and officially registered in 1990. The founding members comprised a professional group of women lawyers who felt the need for an organization that could promote an environment guaranteeing equal rights and access to all by focusing on vulnerable and marginalised groups especially women and children. The founding members also recognised the need for women lawyers to foster mutual support for each other in
professional advancement and social responsibility  in 1954 by an Act of Parliament – the Tanganyika Law Society Ordinance 1954. The Tanganyika Law Society is currently governed by the Tanganyika Law Society Act, Cap 307 R.E. 2002, which repealed the earlier legislation    

</p>
   <figure class="svg-wrapper">
        <svg width="24px" height="24px" viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g id="Partner-Detail-(Sea-Change)" transform="translate(-216.000000, -1022.000000)" fill="#d82146">
            <g id="Org-Links" transform="translate(215.000000, 1008.000000)">
                <path d="M23.0833912,25.1110773 L19.6944935,28.4999751 C19.8056049,27.222194 19.6389378,25.9721907 19.1944922,24.7777431 L20.9722746,22.9721828 C22.3333893,21.6110681 22.3333893,19.3888401 20.9722746,18.0277254 C19.6111599,16.6666107 17.3889319,16.6666107 16.0278172,18.0277254 L12.2778074,21.7499574 C10.9166927,23.1388499 10.9166927,25.3333001 12.2778074,26.7221926 C12.8889201,27.3055275 12.8889201,28.2499744 12.2778074,28.8333093 C11.6944725,29.4166442 10.7500256,29.4166442 10.1666907,28.8333093 C7.6389063,26.277747 7.6389063,22.1666251 10.1666907,19.6388407 L13.8889227,15.9166088 C16.4444849,13.3610465 20.5556068,13.3610465 23.0833912,15.9166088 C25.6389535,18.4443932 25.6389535,22.5555151 23.0833912,25.1110773 Z M15.8333722,32.3610963 L12.0833624,36.1111062 C9.55557799,38.6388906 5.4444561,38.6388906 2.88889384,36.1111062 C0.361109436,33.5555439 0.361109436,29.444422 2.88889384,26.9166376 L6.30556947,23.499962 C6.19445807,24.7777431 6.36112517,26.0555242 6.80557078,27.2499718 L5.02778834,29.0277542 C3.66667366,30.3888689 3.66667366,32.611097 5.02778834,33.9722117 C6.38890302,35.3333263 8.61113107,35.3333263 9.97224575,33.9722117 L13.6944777,30.2499797 C15.0833703,28.888865 15.0833703,26.6666369 13.6944777,25.3055223 C13.1111429,24.7221874 13.1111429,23.7499626 13.6944777,23.1666278 C14.2778126,22.5832929 15.2500374,22.5832929 15.8333722,23.1666278 C18.3611566,25.72219 18.3611566,29.8333119 15.8333722,32.3610963 Z" id="link"></path>
            </g>
        </g>
    </g>
    <span class="asasi-website-link"><a href="https://tls.or.tz
">www.tls.or.tz</a></span>
</svg>
        </figure>
        </div>
        </div>

              <div class="col-lg-12">
           <div class="col-sm-4 asasi-logo-wrapper">
        <img class="center-block" src="./images/associations/RSA.jpg" alt="" style="height:90px">
        </div>
        <div class="col-sm-8 asasi-main-description">
        <h3>Road safety Ambassadors </h3>
        <p class="asas-description">Taasisi ya Mabalozi wa Usalama Barabarani Tanzania (Tanzani Road Safety Ambassadors) inashughulika na kuimarisha usalama barabarani kupitia elimu 
</p>
   <figure class="svg-wrapper">
        <svg width="24px" height="24px" viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g id="Partner-Detail-(Sea-Change)" transform="translate(-216.000000, -1022.000000)" fill="#d82146">
            <g id="Org-Links" transform="translate(215.000000, 1008.000000)">
                <path d="M23.0833912,25.1110773 L19.6944935,28.4999751 C19.8056049,27.222194 19.6389378,25.9721907 19.1944922,24.7777431 L20.9722746,22.9721828 C22.3333893,21.6110681 22.3333893,19.3888401 20.9722746,18.0277254 C19.6111599,16.6666107 17.3889319,16.6666107 16.0278172,18.0277254 L12.2778074,21.7499574 C10.9166927,23.1388499 10.9166927,25.3333001 12.2778074,26.7221926 C12.8889201,27.3055275 12.8889201,28.2499744 12.2778074,28.8333093 C11.6944725,29.4166442 10.7500256,29.4166442 10.1666907,28.8333093 C7.6389063,26.277747 7.6389063,22.1666251 10.1666907,19.6388407 L13.8889227,15.9166088 C16.4444849,13.3610465 20.5556068,13.3610465 23.0833912,15.9166088 C25.6389535,18.4443932 25.6389535,22.5555151 23.0833912,25.1110773 Z M15.8333722,32.3610963 L12.0833624,36.1111062 C9.55557799,38.6388906 5.4444561,38.6388906 2.88889384,36.1111062 C0.361109436,33.5555439 0.361109436,29.444422 2.88889384,26.9166376 L6.30556947,23.499962 C6.19445807,24.7777431 6.36112517,26.0555242 6.80557078,27.2499718 L5.02778834,29.0277542 C3.66667366,30.3888689 3.66667366,32.611097 5.02778834,33.9722117 C6.38890302,35.3333263 8.61113107,35.3333263 9.97224575,33.9722117 L13.6944777,30.2499797 C15.0833703,28.888865 15.0833703,26.6666369 13.6944777,25.3055223 C13.1111429,24.7221874 13.1111429,23.7499626 13.6944777,23.1666278 C14.2778126,22.5832929 15.2500374,22.5832929 15.8333722,23.1666278 C18.3611566,25.72219 18.3611566,29.8333119 15.8333722,32.3610963 Z" id="link"></path>
            </g>
        </g>
    </g>
    <span class="asasi-website-link"><a href="https://www.facebook.com/groups/rsatz/
">www.facebook.com/groups/rsatz/</a></span>
</svg>
        </figure>
        </div>
        </div>


    <div class="col-lg-12">
        <div class="col-sm-4 asasi-logo-wrapper">
        <img class="center-block" src="./images/associations/tcrf.png" alt="" style="height:83px">
        </div>
        <div class="col-sm-8 asasi-main-description">
        <h3>Tanzania Child Rights Forum </h3>
        <p class="asas-description">
        Tanzania Child Rights Forum (TCRF) is a platform with open membership committed to credibly support the fulfillment of children’s rights in Tanzania. Formed in 2009 along with Law of the Child Act 2009, the TCRF has reached about 85 Tanzanian Civil Society Organisation (CSOs) working on Child Rights issues across the country in Tanzania to date.

The TCRF intends to take lead in coordinating CSO initiatives to support the Government in its efforts to implement the Law of the Child Act, international child rights instruments and the recommendations of international child rights treaty bodies to Tanzania.



</p>
   <figure class="svg-wrapper">
        <svg width="24px" height="24px" viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g id="Partner-Detail-(Sea-Change)" transform="translate(-216.000000, -1022.000000)" fill="#d82146">
            <g id="Org-Links" transform="translate(215.000000, 1008.000000)">
                <path d="M23.0833912,25.1110773 L19.6944935,28.4999751 C19.8056049,27.222194 19.6389378,25.9721907 19.1944922,24.7777431 L20.9722746,22.9721828 C22.3333893,21.6110681 22.3333893,19.3888401 20.9722746,18.0277254 C19.6111599,16.6666107 17.3889319,16.6666107 16.0278172,18.0277254 L12.2778074,21.7499574 C10.9166927,23.1388499 10.9166927,25.3333001 12.2778074,26.7221926 C12.8889201,27.3055275 12.8889201,28.2499744 12.2778074,28.8333093 C11.6944725,29.4166442 10.7500256,29.4166442 10.1666907,28.8333093 C7.6389063,26.277747 7.6389063,22.1666251 10.1666907,19.6388407 L13.8889227,15.9166088 C16.4444849,13.3610465 20.5556068,13.3610465 23.0833912,15.9166088 C25.6389535,18.4443932 25.6389535,22.5555151 23.0833912,25.1110773 Z M15.8333722,32.3610963 L12.0833624,36.1111062 C9.55557799,38.6388906 5.4444561,38.6388906 2.88889384,36.1111062 C0.361109436,33.5555439 0.361109436,29.444422 2.88889384,26.9166376 L6.30556947,23.499962 C6.19445807,24.7777431 6.36112517,26.0555242 6.80557078,27.2499718 L5.02778834,29.0277542 C3.66667366,30.3888689 3.66667366,32.611097 5.02778834,33.9722117 C6.38890302,35.3333263 8.61113107,35.3333263 9.97224575,33.9722117 L13.6944777,30.2499797 C15.0833703,28.888865 15.0833703,26.6666369 13.6944777,25.3055223 C13.1111429,24.7221874 13.1111429,23.7499626 13.6944777,23.1666278 C14.2778126,22.5832929 15.2500374,22.5832929 15.8333722,23.1666278 C18.3611566,25.72219 18.3611566,29.8333119 15.8333722,32.3610963 Z" id="link"></path>
            </g>
        </g>
    </g>
    <span class="asasi-website-link"><a href="http://www.childrightsforum.org/
">www.childrightsforum.org/</a></span>
</svg>
        </figure>
        </div>
        </div>


        <div class="col-lg-12 asasi-item-wrapper">
        <div class="col-sm-4 asasi-logo-wrapper">
        <img class="center-block" src="./images/associations/TMF.png" alt="">
        </div>
        <div class="col-sm-8 asasi-main-description">
        <h3>Tanzania Media foundation</h3>
        <p class="asas-description">We seek to support and promote investigative and public interest journalism through grants and learning, so that the public is better informed, able to debate and demand greater accountability across Tanzania.</p>
        <figure class="svg-wrapper">
        <svg width="24px" height="24px" viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g id="Partner-Detail-(Sea-Change)" transform="translate(-216.000000, -1022.000000)" fill="#d82146">
            <g id="Org-Links" transform="translate(215.000000, 1008.000000)">
                <path d="M23.0833912,25.1110773 L19.6944935,28.4999751 C19.8056049,27.222194 19.6389378,25.9721907 19.1944922,24.7777431 L20.9722746,22.9721828 C22.3333893,21.6110681 22.3333893,19.3888401 20.9722746,18.0277254 C19.6111599,16.6666107 17.3889319,16.6666107 16.0278172,18.0277254 L12.2778074,21.7499574 C10.9166927,23.1388499 10.9166927,25.3333001 12.2778074,26.7221926 C12.8889201,27.3055275 12.8889201,28.2499744 12.2778074,28.8333093 C11.6944725,29.4166442 10.7500256,29.4166442 10.1666907,28.8333093 C7.6389063,26.277747 7.6389063,22.1666251 10.1666907,19.6388407 L13.8889227,15.9166088 C16.4444849,13.3610465 20.5556068,13.3610465 23.0833912,15.9166088 C25.6389535,18.4443932 25.6389535,22.5555151 23.0833912,25.1110773 Z M15.8333722,32.3610963 L12.0833624,36.1111062 C9.55557799,38.6388906 5.4444561,38.6388906 2.88889384,36.1111062 C0.361109436,33.5555439 0.361109436,29.444422 2.88889384,26.9166376 L6.30556947,23.499962 C6.19445807,24.7777431 6.36112517,26.0555242 6.80557078,27.2499718 L5.02778834,29.0277542 C3.66667366,30.3888689 3.66667366,32.611097 5.02778834,33.9722117 C6.38890302,35.3333263 8.61113107,35.3333263 9.97224575,33.9722117 L13.6944777,30.2499797 C15.0833703,28.888865 15.0833703,26.6666369 13.6944777,25.3055223 C13.1111429,24.7221874 13.1111429,23.7499626 13.6944777,23.1666278 C14.2778126,22.5832929 15.2500374,22.5832929 15.8333722,23.1666278 C18.3611566,25.72219 18.3611566,29.8333119 15.8333722,32.3610963 Z" id="link"></path>
            </g>
        </g>
    </g>
    <span class="asasi-website-link"><a href="https://www.tmf.or.tz">www.tmf.or.tz</a></span>
</svg>
        </figure>
        </div>
        </div>

        <div class="col-lg-12">
        <div class="col-sm-4 asasi-logo-wrapper">
        <img class="center-block" src="./images/associations/wlac.jpeg" alt="">
        </div>
        <div class="col-sm-8 asasi-main-description">
        <h3>Women Legal Aid Centre </h3>
        <p class="asas-description">A non-profit NGO that works to empower women to attain their rights and to improve vulnerable population’s access to justice across Tanzania.
</p>
   <figure class="svg-wrapper">
        <svg width="24px" height="24px" viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g id="Partner-Detail-(Sea-Change)" transform="translate(-216.000000, -1022.000000)" fill="#d82146">
            <g id="Org-Links" transform="translate(215.000000, 1008.000000)">
                <path d="M23.0833912,25.1110773 L19.6944935,28.4999751 C19.8056049,27.222194 19.6389378,25.9721907 19.1944922,24.7777431 L20.9722746,22.9721828 C22.3333893,21.6110681 22.3333893,19.3888401 20.9722746,18.0277254 C19.6111599,16.6666107 17.3889319,16.6666107 16.0278172,18.0277254 L12.2778074,21.7499574 C10.9166927,23.1388499 10.9166927,25.3333001 12.2778074,26.7221926 C12.8889201,27.3055275 12.8889201,28.2499744 12.2778074,28.8333093 C11.6944725,29.4166442 10.7500256,29.4166442 10.1666907,28.8333093 C7.6389063,26.277747 7.6389063,22.1666251 10.1666907,19.6388407 L13.8889227,15.9166088 C16.4444849,13.3610465 20.5556068,13.3610465 23.0833912,15.9166088 C25.6389535,18.4443932 25.6389535,22.5555151 23.0833912,25.1110773 Z M15.8333722,32.3610963 L12.0833624,36.1111062 C9.55557799,38.6388906 5.4444561,38.6388906 2.88889384,36.1111062 C0.361109436,33.5555439 0.361109436,29.444422 2.88889384,26.9166376 L6.30556947,23.499962 C6.19445807,24.7777431 6.36112517,26.0555242 6.80557078,27.2499718 L5.02778834,29.0277542 C3.66667366,30.3888689 3.66667366,32.611097 5.02778834,33.9722117 C6.38890302,35.3333263 8.61113107,35.3333263 9.97224575,33.9722117 L13.6944777,30.2499797 C15.0833703,28.888865 15.0833703,26.6666369 13.6944777,25.3055223 C13.1111429,24.7221874 13.1111429,23.7499626 13.6944777,23.1666278 C14.2778126,22.5832929 15.2500374,22.5832929 15.8333722,23.1666278 C18.3611566,25.72219 18.3611566,29.8333119 15.8333722,32.3610963 Z" id="link"></path>
            </g>
        </g>
    </g>
    <span class="asasi-website-link"><a href="http://wlac.or.tz/">www.wlac.or.tz</a></span>
</svg>
        </figure>
        </div>
        </div>



  <div class="col-lg-12">
        <div class="col-sm-4 asasi-logo-wrapper">
        <img class="center-block" src="./images/associations/amend.gif" style="height:33px" alt="">
        </div>
        <div class="col-sm-8 asasi-main-description">
        <h3>Amend Tanzania</h3>
        <p class="asas-description">Amend develops, implements and evaluates evidence-based programs to reduce the incidence of road traffic injury in Africa. We run our own road safety programs and partner with governments, companies, development agencies, and others on projects that target specific aspects of road traffic injury
</p>
   <figure class="svg-wrapper">
        <svg width="24px" height="24px" viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g id="Partner-Detail-(Sea-Change)" transform="translate(-216.000000, -1022.000000)" fill="#d82146">
            <g id="Org-Links" transform="translate(215.000000, 1008.000000)">
                <path d="M23.0833912,25.1110773 L19.6944935,28.4999751 C19.8056049,27.222194 19.6389378,25.9721907 19.1944922,24.7777431 L20.9722746,22.9721828 C22.3333893,21.6110681 22.3333893,19.3888401 20.9722746,18.0277254 C19.6111599,16.6666107 17.3889319,16.6666107 16.0278172,18.0277254 L12.2778074,21.7499574 C10.9166927,23.1388499 10.9166927,25.3333001 12.2778074,26.7221926 C12.8889201,27.3055275 12.8889201,28.2499744 12.2778074,28.8333093 C11.6944725,29.4166442 10.7500256,29.4166442 10.1666907,28.8333093 C7.6389063,26.277747 7.6389063,22.1666251 10.1666907,19.6388407 L13.8889227,15.9166088 C16.4444849,13.3610465 20.5556068,13.3610465 23.0833912,15.9166088 C25.6389535,18.4443932 25.6389535,22.5555151 23.0833912,25.1110773 Z M15.8333722,32.3610963 L12.0833624,36.1111062 C9.55557799,38.6388906 5.4444561,38.6388906 2.88889384,36.1111062 C0.361109436,33.5555439 0.361109436,29.444422 2.88889384,26.9166376 L6.30556947,23.499962 C6.19445807,24.7777431 6.36112517,26.0555242 6.80557078,27.2499718 L5.02778834,29.0277542 C3.66667366,30.3888689 3.66667366,32.611097 5.02778834,33.9722117 C6.38890302,35.3333263 8.61113107,35.3333263 9.97224575,33.9722117 L13.6944777,30.2499797 C15.0833703,28.888865 15.0833703,26.6666369 13.6944777,25.3055223 C13.1111429,24.7221874 13.1111429,23.7499626 13.6944777,23.1666278 C14.2778126,22.5832929 15.2500374,22.5832929 15.8333722,23.1666278 C18.3611566,25.72219 18.3611566,29.8333119 15.8333722,32.3610963 Z" id="link"></path>
            </g>
        </g>
    </g>
    <span class="asasi-website-link"><a href="https://www.amend.org/ ">www.amend.org</a></span>
</svg>
        </figure>
        </div>
        </div>

        <div class="col-lg-12">
        <div class="col-sm-4 asasi-logo-wrapper">
        <img class="center-block" src="./images/associations/taboa.png" alt="" style="height:43px">
        </div>
        <div class="col-sm-8 asasi-main-description">
        <h3>TABOA </h3>
        <p class="asas-description">
</p>
   <figure class="svg-wrapper">
        <svg width="24px" height="24px" viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g id="Partner-Detail-(Sea-Change)" transform="translate(-216.000000, -1022.000000)" fill="#d82146">
            <g id="Org-Links" transform="translate(215.000000, 1008.000000)">
                <path d="M23.0833912,25.1110773 L19.6944935,28.4999751 C19.8056049,27.222194 19.6389378,25.9721907 19.1944922,24.7777431 L20.9722746,22.9721828 C22.3333893,21.6110681 22.3333893,19.3888401 20.9722746,18.0277254 C19.6111599,16.6666107 17.3889319,16.6666107 16.0278172,18.0277254 L12.2778074,21.7499574 C10.9166927,23.1388499 10.9166927,25.3333001 12.2778074,26.7221926 C12.8889201,27.3055275 12.8889201,28.2499744 12.2778074,28.8333093 C11.6944725,29.4166442 10.7500256,29.4166442 10.1666907,28.8333093 C7.6389063,26.277747 7.6389063,22.1666251 10.1666907,19.6388407 L13.8889227,15.9166088 C16.4444849,13.3610465 20.5556068,13.3610465 23.0833912,15.9166088 C25.6389535,18.4443932 25.6389535,22.5555151 23.0833912,25.1110773 Z M15.8333722,32.3610963 L12.0833624,36.1111062 C9.55557799,38.6388906 5.4444561,38.6388906 2.88889384,36.1111062 C0.361109436,33.5555439 0.361109436,29.444422 2.88889384,26.9166376 L6.30556947,23.499962 C6.19445807,24.7777431 6.36112517,26.0555242 6.80557078,27.2499718 L5.02778834,29.0277542 C3.66667366,30.3888689 3.66667366,32.611097 5.02778834,33.9722117 C6.38890302,35.3333263 8.61113107,35.3333263 9.97224575,33.9722117 L13.6944777,30.2499797 C15.0833703,28.888865 15.0833703,26.6666369 13.6944777,25.3055223 C13.1111429,24.7221874 13.1111429,23.7499626 13.6944777,23.1666278 C14.2778126,22.5832929 15.2500374,22.5832929 15.8333722,23.1666278 C18.3611566,25.72219 18.3611566,29.8333119 15.8333722,32.3610963 Z" id="link"></path>
            </g>
        </g>
    </g>
    <span class="asasi-website-link"><a href="http://www.taboa.or.tz/
">www.taboa.or.tz</a></span>
</svg>
        </figure>
        </div>
        </div>


        </div>

        </div>
    </div>


    @component('shared.footer')
@endcomponent
@endsection
