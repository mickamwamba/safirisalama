@extends('layouts.admin_login')
@section('content')
<div class="container-fluid">
    <div class="row welcome-wrapper">
    <div class="col-md-6 logIn-hero" style="background-repeat: no-repeat;text-align: center;background-size: cover; background-color:#d92246">
            <h1> 
                Safiri Salama   <br>
               Admin Panel
            </h1>
        </div>
        <div class="col-md-6 form-wrapper">
            <div class="col-md-8 col-md-offset-2">
                <h1>Welcome</h1>
                <p class="form-intro">Lets get signed In!</p>
                <form class="form-horizontal" role="form" method="POST" action="{{ route('admin.loginpost') }}">
                    {{ csrf_field() }}
                   
                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }} label-static is-empty text-inputs">
                        <label for="i2" class="control-label">Phone</label>
                        <input type="text" name="phone" value="{{ old('phone') }}" class="form-control main-field" id="i2" placeholder="Enter your phone number" required>
                          @if ($errors->has('phone'))
                                      <span class="error" style="color: #dd5252">
                                        <strong><?php echo $errors->first('phone');?></strong>
                                    </span>
                          @endif

                    <span style="color:#c70707">@if(session('error')) {{ session('error')}}  @endif</span>
                          
                    </div>
                    
                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }} label-static is-empty text-input-last">
                        <label for="i2" class="control-label">Password</label>
                        <input type="password" name="password" class="form-control main-field" id="i2" placeholder="Password goes here" required>
                    
                           @if ($errors->has('password'))
                                      <span class="error" style="color: #dd5252">
                                        <strong>{{$errors->first('email')}}</strong>
                                    </span>
                                @endif
                    </div>

                    <div class="form-group">
                        <div class="checkbox">
                            <label class="auto-signIn">
                                <input type="checkbox"><span class="checkbox-material"></span> Keep me signed In.
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                            <button type="submit" style="background-color:#d92246;" class="btn btn-raised btn-success btn-post btn-custom" name="submit" value="submit">SIGN IN</button>
                    </div>
                    
                </form>
                <p class="recover-details">Forgot password?<a href="#" data-toggle="modal" data-target="#myModal" class="recover-btn">Click here</a></p>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div class="header-wrapper">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Password recovery</h4>
                </div>
            </div>
            <form action="">
                <div class="modal-body">
                    <p>Recover password easily by entering your recovery email</p>
                    <div class="form-group label-placeholder is-empty">
                        <label for="i5p" class="control-label">Enter your recovery Email here.</label>
                        <input type="email" class="form-control" id="i5p">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection