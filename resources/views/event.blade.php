@extends('layouts.client')
@section('title','Event Title')
@section('content')
@component('shared.navbar')
@slot('navbarClass')
    navbar-solid
@endslot
@slot('btnClass')
    action-btn
@endslot

@slot('buttonTheme','btn-theme')

@endcomponent
<div class="event-intro-block" style="background-image:url({{count($event->media)>0?$event->media[0]->media:''}})">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
            </div>
        </div>
    </div>
</div>
<div class="safiri-event-content-block ">
        <div class="container">
                <div class="row">
                <div class="col-lg-12 event-name-col">
                <div class="event-name-wrapper">
                   <h4>{{$event->title}}</h4>
                </div>    
                </div>
                </div>
                <div class="row">
                <div class="col-lg-12 news-content-col">
                         <div class="row events-wrapper">
                <div class="col-sm-4 event-bar event-bar-date">
                    <h5 class="event-date-header">Date</h5>
                    <h6 class="event-light-headers">Start Date</h6>
                    <h5>{{date("D, M,",strtotime($event->start_date))}}
                        <br>{{date("d, Y",strtotime($event->start_date))}}</h5>
                    <h6 class="event-light-headers">End Date</h6>
                    <h5>{{date("D, M,",strtotime($event->end_date))}}
                        <br>{{date("d, Y",strtotime($event->end_date))}}</h5>
                </div>
                <div class="col-sm-4 event-bar event-bar-time">
                    <h5 class="event-date-header">Time</h5>
                    <h6 class="event-light-headers">Start Time</h6>
                    <h5>12:30 PM</h5>
                    <h6 class="event-light-headers">End Date</h6>
                    <h5>7:00 PM</h5>
                </div>
                <div class="col-sm-4 event-bar event-bar-location">
                    <h5 class="event-date-header">Location</h5>
                    <h5>{{$event->venue}}</h5>
                </div>
            </div>
                </div>
                </div>
                <div class="row fee-row">
                <div class="fee-wrapper">
                    <div class="col-xs-6">
                    <h5>Fee</h5>
                    </div>
                    <div class="col-xs-6">
                    <h5 class="right-align">Free</h5>
                    </div>
                </div>
                </div>
                  <div class="row desc-row">
                <div class="desc-wrapper">
                    <div class="col-xs-12 desc-header-wrapper">
                    <h5 class="bolded-header">Description</h5>
                    </div>
                </div>
                </div>
                <div class="row desc-content-row">
                <div class="desc-content-wrapper">
                    <div class="col-xs-12">
                <p class="paragraph">
                                    {{$event->description}}
                </p>
                    </div>
                </div>
                </div>
            </div>
        </div>
@component('shared.footer')
@endcomponent
@endsection