  <script>
var ctx = document.getElementById("myChart").getContext('2d');
var data = {
  labels: ["2014", "2015", "2016", "2017"],
  datasets: [{
    label: "Ajali",
    backgroundColor: "#FF5A5F",
    data: [3106, 2905, 10297,6022]
  }, {
    label: "Vifo",
    backgroundColor: "#048A81",
    data: [3857, 3574, 3381,2705]
  }, {
    label: "Majeruhi",
    backgroundColor: "#0874B2",
    data: [15230, 9993, 9549,6169]
  }]
};

var myChart = new Chart(ctx, {
    type: 'bar',
    data: data,
    options: {
        legend:{
            display:true
        },
        responsive:true,
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
</script>