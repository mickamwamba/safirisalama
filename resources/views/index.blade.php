@extends('layouts.client')
@section('title','Ajali Sasa Basi')
@section('content')
@component('shared.navbar')
@slot('navbarClass')
    navbar-solid  inverted
@endslot

@slot('btnClass')
    action-btn
@endslot
@slot('buttonTheme')
    button-theme
@endslot
@endcomponent
{{--  main content starts off here  --}}
<div class="main-block">

            <div class="main-wrapper">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2 hero-wrapper">
                            <div class="opening-title">
                            <h2>Kampeni ya umoja wa asasi za kiraia unaolenga  kuimarishwa kwa sera na sheria ya usalama barabarani.</h2>
                            <h3 class="safiri-subheader">Pamoja tunaweza kupunguza vifo  na ulemavu unaosababishwa na ajali za barabarani ifikapo 2020.</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
 </div>
 <div class="commitment-block">
     <div class="container">
         <div class="row">
             <div class="col-sm-6">
                 <div class="commitment-wrapper">
                    <h3 class="bright-header">#Ajalisasabasi</h3>
                    <h4 class="dark-header">Tuseme  #Ajalisasabasi</h4>
                    </div>
             </div>
             <div class="col-sm-6 big-action-btn-wrapper">
                      <a href="/take_action">
          <div class="btn btn-default navbar-btn big-action-btn ">
           <span class="btn-title">CHUKUA HATUA</span>
          </div>
        </a>
             </div>
         </div>
     </div>
 </div>
 <div class="carousel-block">
<div class="container">
<h3 class="mapendekezo-title">Mapendekezo ya kurekebisha sheria</h3>
</div>
 <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
    <li data-target="#carousel-example-generic" data-slide-to="3"></li>
    <li data-target="#carousel-example-generic" data-slide-to="4"></li>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
    <div class="item active">
      <img class="img-responsive caro-image center-block" src="./images/cartoons/cartoon1.jpg" alt="">
  
    </div>
    <div class="item">
      <img class="img-responsive caro-image center-block" src="./images/cartoons/cartoon2.jpg" alt="">
  
    </div>
      <div class="item">
      <img class="img-responsive caro-image center-block" src="./images/cartoons/cartoon3.jpg" alt="">
     
    </div>
      <div class="item">
      <img class="img-responsive caro-image center-block" src="./images/cartoons/cartoon4.jpg" alt="">
   
    </div>
      <div class="item">
      <img class="img-responsive caro-image center-block" src="./images/cartoons/cartoon5.jpg" alt="">
  
    </div>
  </div>

  <!-- Controls -->
  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
 </div>
 <div class="stats-block">
     <div class="stats-block-wrapper">
     <div class="container">
               <h2 class="center-takwimu-header bright-header">Takwimu za ajali barabarani</h2>
                <p class="center-subheader bright-header">Taarifa kutoka jeshi la polisi Tanzania 2017</p>
         <div class="row stats-wrapper">
             <div class="col-sm-4">
                 <h2 class="stats-number"> 6,022</h2>
                <h4>Ajali</h4>
             </div>
             <div class="col-sm-4">
                  <h2 class="stats-number safiri-text">2,705</h2>
                <h4 class="safiri-text">Vifo</h4>
             </div>
             <div class="col-sm-4">
                  <h2 class="stats-number"> 6,169 </h2>
                <h4>Majeruhi</h4>
             </div>
         </div>
         <div class="see-more-wrapper">
             <a href="/stats" class="btn btn-primary see-more-btn">
                 ONA ZAIDI
             </a>
         </div>
     </div>
     </div>
 </div>
{{--  news block  --}}
<div class="news-block">
    <h2 class="center-header safiri-text">
        News
    </h2>
    <div class="container">
        <div class="row justify-content-center">

                @foreach ($news as $n)
                <?php 
                $re = '/(h.*v=)(.{11})/';
                $str = $n->video;
                preg_match_all($re, $str, $matches, PREG_SET_ORDER, 0);
               
                $video_id = $str!=NULL&&count($matches)>0?$matches[0][2]:'';
                $thumbnail = 'https://img.youtube.com/vi/'.$video_id.'/0.jpg';
                ?>
                @component('shared.news')
                    @slot('link')
                    {{$n->id}}
                    @endslot
                    @slot('thumbnail')
                    {{$thumbnail}}
                    @endslot
                    @slot('newsDate')
                    {{date("d",strtotime($n->created_at))}}
                    @endslot
                    @slot('newsMonth')
                    {{date("M",strtotime($n->created_at))}}
                    @endslot
                    @slot('newsTitle')
                    {{--  <a href="news"> Ut rerum distinctio fuga cupiditate est.</a>  --}}
                   {{$n->title}}
                    @endslot
                    @slot('newsAuthor')
                    Admin
                    @endslot
                @endcomponent
                @endforeach

        </div>
    </div>
</div>
{{-- social media block --}}
<div class="social-block">
    <h2 class="center-header black-header">Jiunge <span class="safiri-text">nasi Mitandaoni</span></h2>
    <div class="container">
        <div class="row">
            <div class="col-sm-3 social-icon">
                <a href="https://web.facebook.com/Ajalisasabasi/"><i class="fa fa-facebook-official" aria-hidden="true"></i></a>
            </div>
            <div class="col-sm-3 social-icon">
                <a href="https://twitter.com/safiri_salama"><i class="fa fa-twitter-square" aria-hidden="true"></i></a>
            </div>
            <div class="col-sm-3 social-icon">
                <a href="https://www.instagram.com/safirisalama/"><i class="fa fa-instagram" aria-hidden="true"></i></a>
            </div>
            <div class="col-sm-3 social-icon">
                <a href="https://www.youtube.com/channel/UCz3izliYASJpfIlXxN0VJBQ"><i class="fa fa-youtube-play" aria-hidden="true"></i></a>
            </div>
        </div>
    </div>
</div>
@component('shared.footer')
@endcomponent
@endsection
