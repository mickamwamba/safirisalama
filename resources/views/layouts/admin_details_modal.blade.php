  <div class="modal fade" id="accountModal" tabindex="-1" role="dialog" aria-labelledby="accountModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <div class="header-wrapper">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="myModalLabel">Account - {{Auth::user()->fname}} {{Auth::user()->sname}}</h4>
            </div>
          </div>
          <div class="subnavbar">
            <ul  class="nav nav-pills">
              <li class="active">
                <a  href="#info" data-toggle="tab">INFO</a>
              </li>
              <li><a href="#pass" data-toggle="tab">CHANGE PASSWORD</a>
            </li>
          </ul>
        </div>
        <div class="container" id="exTab1">
          <div class="tab-content clearfix">
            <div class="tab-pane active" id="info">
              <form method="POST" enctype="multipart/form-data" action="{{route('admin.update_details')}}">
                {{ csrf_field() }}
                <div class="modal-body">
                  <p>Update your account information here.</p>
                 <div class="form-group label-placeholder is-empty">
                   
                    <input type="text" name="fname" value="{{Auth::user()->fname}}" class="form-control" id="i5p" required placeholder="First name">
                  </div>
                     <div class="form-group label-placeholder is-empty">
              
                    <input type="text" name="mname" value="{{Auth::user()->mname}}" class="form-control" id="i5p" placeholder="Middle name">
                  </div>
                     <div class="form-group label-placeholder is-empty">
    
                    <input type="text" name="sname" value="{{Auth::user()->sname}}" class="form-control" id="i5p" placeholder="Surname" required>
                  </div>
                  <div class="form-group label-placeholder is-empty">
                   
                    <input type="text" name="phone" value="{{Auth::user()->phone}}" class="form-control" id="i5p" placeholder="Phone" required disabled>
                  </div>
                     <div class="form-group label-placeholder is-empty">
                   
                    <input type="text" name="email" value="{{Auth::user()->email}}" class="form-control" id="i5p" placeholder="Email address" disabled>
                  </div>
                   <input type="hidden" name="admin_id" value="{{Auth::user()->id}}" class="form-control" required>
                </div>
                <div class="modal-footer">
                  <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
              </form>
              
            </div>
            <div class="tab-pane " id="pass">
              <form method="POST" enctype="multipart/form-data" action="{{route('admin.update_password')}}">
                {{ csrf_field() }}
                <div class="modal-body">
                  <p>Change your password here.</p>
                  <div class="form-group label-placeholder is-empty">
                    <label for="password" class="control-label">Current password</label>
                    <input type="password" name="current_password" class="form-control" id="i5p">
                  </div>
                     <div class="form-group label-placeholder is-empty">
                    <label for="password" class="control-label">New password</label>
                    <input type="password" name="new_password" class="form-control" id="i5p">
                  </div>
                     <div class="form-group label-placeholder is-empty">
                    <label for="password" class="control-label">Confirm new password</label>
                    <input type="password" name="confirm_password" class="form-control" id="i5p">
                  </div>
                  <input type="hidden" name="admin_id" value="{{Auth::user()->id}}">
                                    
                <div class="modal-footer">
                  <button type="submit" class="btn btn-primary">PROCEED</button>
                  
                </div>
              </form>
            </div>
          </div>
        </div>
        
      </div>
    </div>
  </div>