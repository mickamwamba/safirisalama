<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
  
    <meta name="theme-color" content="#ffffff">

    <title>SafiriSalama</title>
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="{{url('css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{url('css/bootstrap-material-design.min.css')}}">
    <link rel="stylesheet" href="{{url('css/bootstrap-tagsinput.css')}}">
    <link rel="stylesheet" href="{{url('css/jasny-bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{url('css/helper.css')}}">
    <link rel="stylesheet" href="{{url('css/navmenu.css')}}">
    <link rel="stylesheet" href="{{url('css/main.css')}}">
    <link rel="stylesheet" href="{{url('css/ripples.min.css')}}">
    <link rel="stylesheet" href="{{url('css/animate.css')}}">
    <link rel="stylesheet" href="{{url('css/jquery.dropdown.css')}}">
    <link rel="stylesheet" href="{{url('css/bootstrap-material-datetimepicker.css')}}">
    <link rel="stylesheet" href="{{url('css/tabs.css')}}">
    <link rel="stylesheet" href="{{url('css/book.css')}}">
    <link rel="stylesheet" href="{{url('css/profile.css')}}">

  </head>
  <body>
    <div class="navmenu navmenu-default navmenu-side navmenu-fixed-left offcanvas-sm agri-menu">
      <!--<a class="navmenu-brand visible-md visible-lg" href="#">Agri Salama Info Admin</a>-->
      <div class="user-header" data-toggle="modal" data-target="#accountModal">
        <div class="information">
          <div class="avatar" style="background-image:url('http://www.valuetime.co.in/images/change-user.png')"></div>
          @if(Auth::user())
          <p>{{Auth::user()->fname}}  {{Auth::user()->sname}}</p>
          @endif
        </div>
      </div>
    <ul class="nav navmenu-nav">
      <li class="active withripple"><a href="{{url('news')}}"><i class="fa fa-newspaper-o"></i> Habari</a></li>
      <li><a class="withripple" href="{{url('asasi')}}"> <i class="fa fa-users"></i> Asasi za kiraia</a></li> 
      
      <li><a class="withripple" href="{{url('ripoti')}}">  <i class="fa fa-book"></i> Ripoti</a></li>                 
      {{-- <li><a class="withripple" href="{{url('events')}}">Events</a></li>
      <li><a class="withripple" href="{{url('advocates')}}">Advocate requests</a></li>
      <li><a class="withripple" href="{{url('site')}}">Site contents</a></li>       --}}
      <li><a class="withripple" href="{{url('/logout')}}"> <i class="fa fa-mail-reply"></i> Log out</a></li>
    </ul>
  </div>

  <div class="navbar navbar-default navbar-custom navbar-fixed-top hidden-md hidden-lg ">
    <button type="button" class="navbar-toggle" data-toggle="offcanvas" data-target=".navmenu">
    <a href="#"><i class="fa fa-bars menu-btn" aria-hidden="true"></i></a>

    <div class="navbar navbar-default navbar-custom navbar-fixed-top hidden-md hidden-lg ">
      <button type="button" class="navbar-toggle" data-toggle="offcanvas" data-target=".navmenu">
      <a href="#"><i class="fa fa-bars menu-btn" aria-hidden="true"></i></a>
      </button>
      <a class="navbar-brand" href="#">SafiriSalama</a>
    </div>
    <div class="toolbar hidden-sm hidden-xs">
      <div class="toolbar-wrapper">
      
          <img alt="Brand" class="navbar-brand-img" src="{{url('/images/Logo/brand.png')}}">
    
        <p class="toolbar-title">SafiriSalama</p>
        
          @if (session('msg'))
    <div class="feedbackmsg">
    
        <div class="modal fade bs-example-modal-lg" id="feedbackmsg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content message" style="left: 100px;height: 94px;padding: 39px;text-transform: uppercase;color: #3d2c56;text-align: center;font-size: 17px;">
       {{ session('msg') }}
    </div>
  </div>
</div>
    </div>
@endif
      </div>
    </div>
    @yield('content')
    @include('layouts.admin_details_modal')
   
  <!-- JavaScripts -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js" integrity="sha384-I6F5OKECLVtK/BL+8iSLDEHowSAfUo76ZL9+kGAgTRdiByINKJaqTPH/QVNS1VDb"
  crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS"
  crossorigin="anonymous"></script>
  <script type="text/javascript" src="{{url('js/lib/jasny-bootstrap.min.js')}}"></script>
  <script type="text/javascript" src="{{url('js/lib/ripples.min.js')}}"></script>
  <script src="https://use.fontawesome.com/9cf45c8a2f.js"></script>
  <script type="text/javascript" src="{{url('js/lib/material.min.js')}}"></script>
  <script type="text/javascript" src="{{url('js/lib/materialize.min.js')}}"></script>
  <script type="text/javascript" src="{{url('js/lib/snackbar.min.js')}}"></script>
  <script type="text/javascript" src="{{url('js/lib/moment.js')}}"></script>
  <script type="text/javascript" src="{{url('js/lib/bootstrap-material-datetimepicker.js')}}"></script>
  {{-- <script type="text/javascript" src="{{url('js/lib/jquery.dropdown.js')}}"></script> --}}
  <script type="text/javascript" src="{{url('js/lib/bootstrap-tagsinput.min.js')}}"></script>
  <script type="text/javascript">
  $.material.init();
  </script>
  {{--
  <script src="{{ elixir('js/app.js') }}"></script> --}}
  <script type="text/javascript">
  $.material.init();
  $(document).ready(function () {
  $('#feedbackmsg').modal('show');
  $('#date').bootstrapMaterialDatePicker({
  time: false,
  clearButton: true
  });
  $(".select").dropdown({
  autoinit: "select"
  });
  $('#date-end').bootstrapMaterialDatePicker
  ({
  weekStart: 0, format: 'DD/MM/YYYY HH:mm'
  });
  $('#date-start').bootstrapMaterialDatePicker
  ({
  weekStart: 0, format: 'DD/MM/YYYY HH:mm', shortTime : true
  }).on('change', function(e, date)
  {
  $('#date-end').bootstrapMaterialDatePicker('setMinDate', date);
  });
  });
  </script>
</body>
</html>