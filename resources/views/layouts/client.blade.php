<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
  
    <meta name="theme-color" content="#ffffff">
    <link rel="icon" type="img/ico" href="{{url('images/icons/favicon.png')}}">
    <title>SafiriSalama | @yield('title')</title>
    <!-- Bootstrap core CSS -->
    
    {{-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous"> --}}
    <link rel="stylesheet" href="{{url('css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{url('css/jasny-bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{url('css/animate.css')}}">
    <link rel="stylesheet" href="{{url('css/offcanvas.css')}}">
    <link rel="stylesheet" href="{{url('css/circle.css')}}">
    <link rel="stylesheet" href="{{url('css/clientLayout.css')}}">
    <link rel="stylesheet" href="{{url('css/take_action.css')}}">
    <link rel="stylesheet" href="{{url('css/resources.css')}}">
    
    <link rel="stylesheet" href="{{url('css/events.css')}}">
    <link rel="stylesheet" href="{{url('css/news.css')}}">
    <link rel="stylesheet" href="{{url('css/bootstrap-social.css')}}">

  </head>
  <body>
  {{--  side menu (only visibile on small devices)  --}}
  <div class="navmenu navmenu-default navmenu-fixed-left visible-xs side-nav-custom">
      <a class="navmenu-brand" href="#">Safiri Salama</a>
      <ul class="nav navmenu-nav">
       <a href="/allNews">Habari</a>
        <li ><a href="/asasi">Asasi za kiraia</a></li>
        <li ><a href="/sheria">Mapendekezo ya sheria</a></li>
        <li><a href="/stats">Takwimu</a></li>
        <li><a href="/reports">Ripoti</a></li>
      </ul>
    </div>
{{--  end of side menu  --}}
    <div class="canvas">
     @yield('content')
     </div>
  </body>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js" integrity="sha384-I6F5OKECLVtK/BL+8iSLDEHowSAfUo76ZL9+kGAgTRdiByINKJaqTPH/QVNS1VDb"
  crossorigin="anonymous"></script>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS"
  crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.bundle.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js"></script>
  <script type="text/javascript" src="{{url('js/lib/jasny-bootstrap.min.js')}}"></script>
  <script type="text/javascript" src="{{url('js/lib/ripples.min.js')}}"></script>
  <script src="https://use.fontawesome.com/c3cb1158bc.js"></script>
@include('includes.chartjs')

  </html>

