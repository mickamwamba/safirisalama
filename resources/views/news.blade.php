@extends('layouts.client')
@section('content')
@component('shared.navbar')
@slot('navbarClass')
    navbar-solid
@endslot
@slot('buttonTheme')
    button-theme
@endslot
@slot('btnClass')
    action-btn
@endslot
@endcomponent

<div class="">
    {{-- <div class="center-cropped" style="background-image: url({{$news->image}});">
    </div> --}}
<div class="container allMarginContainer">
        {{-- <h1 style="color: black; font-weight: bold"> {{$news->title}}</h1>
        <div style="font-weight: bold; font-size: 25px; border-bottom: 1px solid rgba(211, 211, 211, 0.31)">
          
            Posted: {{date("d/m/Y")}}
        </div>
    </div>
    <p class="container allMarginContainer" style="text-align:justify" >
        {{$news->content}} --}}

        <?php 
        $re = '/(h.*v=)(.{11})/';
        $str = $news->video;
        preg_match_all($re, $str, $matches, PREG_SET_ORDER, 0);
       
        $video_id = $str!=NULL&&count($matches)>0?$matches[0][2]:'';
        $thumbnail = 'https://img.youtube.com/vi/'.$video_id.'/0.jpg';
        ?>

        <div class="row main-specific-video-wrapper">
            <div class="col-md-8">
                <div class="specific-video-wrapper">
                    <div class="iframe-wrapper">
                    <iframe width="420" height="315"src="https://www.youtube.com/embed/{{$video_id}}?autoplay=1"></iframe>                    
                    </div>

                   <div> <h3 class="specific-video-title">{{strtoupper($news->title)}}</h3> </div>
                   <div class="specific-video-description">
                   <p>{!!($news->content)!!}</p>
                   </div>
                </div>

            </div>
            <div class="col-md-4 ">
                @foreach($news_list as $n)
                <?php 
                $re = '/(h.*v=)(.{11})/';
                $str = $n->video;
                preg_match_all($re, $str, $matches, PREG_SET_ORDER, 0);
               
                $video_id = $str!=NULL&&count($matches)>0?$matches[0][2]:'';
                $thumbnail = 'https://img.youtube.com/vi/'.$video_id.'/0.jpg';
                ?>
                
                <div class="left-single-video-wrapper">
                        <a href="/news/{{$n->id}}"> <div class="media">
                            <div class="media-left">
                                <img src="{{$thumbnail}}" class="media-object" style="width:100px">
                            </div>
                            <div class="media-body video-thumbnail">
                            <h4 class="media-heading left-video-wrapper">{{strtoupper(substr($n->title,0,40))}}{{(strlen($n->title)>40)?'...':''}}</h4>
                                {{-- <p>Lorem ipsum...</p> --}}
                            </div>
                        </div>
                        </a>
                </div>
                @endforeach
            </div>
        </div>
    </p>
</div>

@component('shared.footer')
@endcomponent

@endsection