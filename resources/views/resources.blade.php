@extends('layouts.client')
@section('content')
@component('shared.navbar')
@slot('navbarClass')
    navbar-solid inverted
@endslot
@slot('btnClass')
    action-btn
@endslot
@slot('buttonTheme')
    button-theme
@endslot
@endcomponent

<div class="main-resources-wrapper">
  <div class="container">

        @if (Session::has('msg'))
        <div class="alert alert-info" style="text-align:center">{{ Session::get('msg') }}</div>
        @endif
       <div class="row">
           <div class="col-md-8 col-md-offset-2">
            <div class="resources-wrapper">

                @foreach($resources as $resource)

                <div class="resource-document">
                        <div class="media">
                            <div class="media-left">
                            <img src="images/icons/{{$resource->type}}.png" class="media-object" style="width:60px">
                            </div>
                                <div class="media-body">
                            <a href="{{$resource->path}}">
                                <h4 class="media-heading">{{$resource->title}}</h4>
                            </a>
                        <div><p>{!!$resource->description!!}</p></div>
                                <p class="upload-date">Uploaded on {{date("d/m/Y",strtotime($resource->created_at))}}</p>
                            </div>

                        <a href="{{$resource->path}}"><span class="download-btn"><img src="images/icons/download.png"> </span></a>
                        </div>
                </div>

                @endforeach
                {{$resources->render()}}
                {{-- <div class="resource-document">
                        <div class="media">
                            <div class="media-left">
                                <img src="images/icons/word.png" class="media-object" style="width:60px">
                            </div>
                            <div class="media-body">
                                <h4 class="media-heading">Global Report on road safety</h4>
                                <p class="upload-date">Uploaded on 21/03/2018</p>
                            </div>
                            <span class="download-btn"> <img src="images/icons/download.png"> </span>
                            
                        </div>
                </div>

                <div class="resource-document">
                        <div class="media">
                            <div class="media-left">
                                <img src="images/icons/excel.png" class="media-object" style="width:60px">
                            </div>
                            <div class="media-body">
                                <h4 class="media-heading">Global Report on road safety</h4>
                                <p class="upload-date">Uploaded on 21/03/2018</p>
                            </div>
                            <span class="download-btn"> <img src="images/icons/download.png"> </span>                            
                        </div>
                </div>

                <div class="resource-document">
                        <div class="media">
                            <div class="media-left">
                                <img src="images/icons/pdf.png" class="media-object" style="width:60px">
                            </div>
                            <div class="media-body">
                                <h4 class="media-heading">Global Report on road safety Global Report on road safety</h4>
                                <p class="upload-date">Uploaded on 21/03/2018</p>
                            </div>
                            <span class="download-btn"> <img src="images/icons/download.png"> </span>
                        </div>
                </div>

                <div class="resource-document">
                        <div class="media">
                            <div class="media-left">
                                <img src="images/icons/word.png" class="media-object" style="width:60px">
                            </div>
                            <div class="media-body">
                                <h4 class="media-heading">Global Report on road safety</h4>
                                <p class="upload-date">Uploaded on 21/03/2018</p>
                            </div>
                            <span class="download-btn"> <img src="images/icons/download.png"> </span>
                            
                        </div>
                </div>

                <div class="resource-document">
                        <div class="media">
                            <div class="media-left">
                                <img src="images/icons/excel.png" class="media-object" style="width:60px">
                            </div>
                            <div class="media-body">
                                <h4 class="media-heading">Global Report on road safety</h4>
                                <p class="upload-date">Uploaded on 21/03/2018</p>
                            </div>
                            <span class="download-btn"> <img src="images/icons/download.png"> </span>                            
                        </div>
                </div>
                 --}}
            </div>
           </div>
           
       </div>
  </div>

</div>
 
 @component('shared.footer')
 @endcomponent

 @endsection