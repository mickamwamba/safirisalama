@extends('layouts.client')

<script>

        window.fbAsyncInit = function() {
          FB.init({
            appId      : '145522809563795',
            cookie     : true,
            xfbml      : true,
            version    : 'v2.11'
          });
         
        
          FB.getLoginStatus(function(response) {
              console.log(response.status);
            if (response.status === 'connected') {
              console.log('Logged in.');
            //  console.log(response);
            }
            else {
                //console.log("loggeed out")
             
                //FB.login(function(){}, {scope: 'publish_actions'});

            }
          });
          FB.AppEvents.logPageView();   
            
        };
      
        (function(d, s, id){
           var js, fjs = d.getElementsByTagName(s)[0];
           if (d.getElementById(id)) {return;}
           js = d.createElement(s); js.id = id;
           js.src = "https://connect.facebook.net/en_US/sdk.js";
           fjs.parentNode.insertBefore(js, fjs);
         }(document, 'script', 'facebook-jssdk'));
      </script>

@section('content')
@component('shared.navbar')
@slot('navbarClass')
    navbar-solid
@endslot
@slot('btnClass')
    action-btn
@endslot
@slot('buttonTheme')
    button-theme
@endslot
@endcomponent
<div class="main-wrapper">
        <div class="container">
            <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                            {{-- <div class="share_image" style="background-image: url({{$image}});"></div> --}}
                            <div class="mag-wrap">
                                    <div class="mag">
                                            <p class="headline">
                                                {{$name}}
                                            </p>
                                    </div>
                                </div>

                        </div>
            </div>
            <div class="row">
                    <div class="col-md-4 col-md-offset-4" id="shareFB">
                            <a class="btn btn-block btn-social btn-facebook">
                                <span class="fa fa-facebook"></span> Sambaza Facebook
                            </a>
                       </div>
            </div>
            
        </div>
</div>

@component('shared.footer')
@endcomponent

    {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js" integrity="sha384-I6F5OKECLVtK/BL+8iSLDEHowSAfUo76ZL9+kGAgTRdiByINKJaqTPH/QVNS1VDb"
    crossorigin="anonymous"></script> --}}

@endsection


<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.js"></script>


<script>
 window.fbAsyncInit = function() {
   FB.init({
     appId      : '145522809563795',
     xfbml      : true,
     version    : 'v2.1'
   });
 };

 (function(d, s, id){
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) {return;}
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));
</script>


<script>
        

$('form').submit(function(e){
       e.preventDefault;
   return false;
});

$(document).ready(function(){
   $('#fancy-text').keyup(
       function(){
           
           var value = $(this).val();
           $('.headline').text(value);
       }
   );

   $('#shareFB').on('click', function(){
       html2canvas($(".mag-wrap"), {
       onrendered: function(canvas) {
           var myImage = canvas.toDataURL("image/png");
                   // $('.lightbox').fadeIn(200);
                   // $('.image').attr('src', myImage).fadeIn(200);

                    var data = myImage;

try {
   blob = dataURItoBlob(data);
} catch (e) {
   console.log(e);
}
FB.getLoginStatus(function (response) {
   console.log(response);
   if (response.status === "connected") {
       postImageToFacebook(response.authResponse.accessToken, "Canvas to Facebook/Twitter", "image/png", blob, window.location.href);
   } else if (response.status === "not_authorized") {
       FB.login(function (response) {
           postImageToFacebook(response.authResponse.accessToken, "Canvas to Facebook/Twitter", "image/png", blob, window.location.href);
       }, {scope: "publish_actions"});
   } else {
       FB.login(function (response) {
           postImageToFacebook(response.authResponse.accessToken, "Canvas to Facebook/Twitter", "image/png", blob, window.location.href);
       }, {scope: "publish_actions"});
   }
});


       }
   });

  
   });
   
   $('.closebox').on('click', function(){
       $('.lightbox').css("display","none");
   });
   

   function dataURItoBlob(dataURI) {
   var byteString = atob(dataURI.split(',')[1]);
   var ab = new ArrayBuffer(byteString.length);
   var ia = new Uint8Array(ab);
   for (var i = 0; i < byteString.length; i++) {
       ia[i] = byteString.charCodeAt(i);
   }
   return new Blob([ab], {type: 'image/png'});
}

// $('#shareFB').click(function () {
//     var data = $('#canvas')[0].toDataURL("image/png");
//     console.log(data);
//     try {
//         blob = dataURItoBlob(data);
//     } catch (e) {
//         console.log(e);
//     }
//     FB.getLoginStatus(function (response) {
//         console.log(response);
//         if (response.status === "connected") {
//             // postImageToFacebook(response.authResponse.accessToken, "Canvas to Facebook/Twitter", "image/png", blob, window.location.href);
//         } else if (response.status === "not_authorized") {
//             FB.login(function (response) {
//                 postImageToFacebook(response.authResponse.accessToken, "Canvas to Facebook/Twitter", "image/png", blob, window.location.href);
//             }, {scope: "publish_actions"});
//         } else {
//             FB.login(function (response) {
//                 postImageToFacebook(response.authResponse.accessToken, "Canvas to Facebook/Twitter", "image/png", blob, window.location.href);
//             }, {scope: "publish_actions"});
//         }
//     });
// });

function postImageToFacebook(token, filename, mimeType, imageData, message) {
   var fd = new FormData();
   fd.append("access_token", token);
   fd.append("source", imageData);
   fd.append("no_story", true);

   // Upload image to facebook without story(post to feed)
   $.ajax({
       url: "https://graph.facebook.com/me/photos?access_token=" + token,
       type: "POST",
       data: fd,
       processData: false,
       contentType: false,
       cache: false,
       success: function (data) {
           console.log("success: ", data);

           // Get image source url
           FB.api(
               "/" + data.id + "?fields=images",
               function (response) {
                   if (response && !response.error) {
                       //console.log(response.images[0].source);

                       // Create facebook post using image
                       FB.api(
                           "/me/feed",
                           "POST",
                           {
                               "message": "",
                               "picture": response.images[0].source,
                               "link": window.location.href,
                               "name": 'Look at the cute panda!',
                               "description": message,
                               "privacy": {
                                   value: 'SELF'
                               }
                           },
                           function (response) {
                               if (response && !response.error) {
                                   /* handle the result */
                                   console.log("Posted story to facebook");
                                   console.log(response);

                               }
                           }
                       );
                   }
               }
           );
       },
       error: function (shr, status, data) {
           console.log("error " + data + " Status " + shr.status);
       },
       complete: function (data) {
           console.log('Post to facebook Complete');
           window.location.href = '/take_action';

       }
   });
}


});	

!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');
</script>