
  <div class="col-sm-4 event-card">
    <a href="{{$link}}" style="text-decoration:none">
    <div class="event-item-wrapper">
    <div class="event-item-image" style="background-image:url('{{$eventImage}}')">
    </div>
    <div class="event-item-body">
        <h4 class="event-item-header">{{$eventTitle}}</h4>
        <h5 class="event-item-author">{{$eventDate}}</h5>
        <p class="event-item-content">
           {{$eventContent}}
        </p>
    </div>
    </div>
    </a>
    </div>