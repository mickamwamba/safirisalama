<footer class="footer-block">
   <div class="container">
       <div class="row">
           <div class="col-sm-4">
               <p class="footer-header">Muwezeshaji</p>
               <img src="{{url('/images/Logo/GRSP.png')}}" alt="">
           </div>
           <div class="col-sm-4">
               <p class="footer-header">Ongea nasi</p>
               <ul class="list-group contact-list-group">
                   <li class="contacts-list-item">+255 68 537 4936</li>
                   <li class="contacts-list-item">safirisalama@gmail.com</li>
                   <li class="contacts-list-item">Dar es salaam, Tanzania</li>
               </ul>
           </div>
           <div class="col-sm-4">
               <p class="footer-header">Mitandaoni</p>
               <ul class="list-group social-list-group">
                   <li><a href="https://web.facebook.com/Ajalisasabasi/"><i class="fa fa-facebook-official" aria-hidden="true"></i></a></li>
                   <li><a href="https://twitter.com/safiri_salama"><i class="fa fa-twitter-square" aria-hidden="true"></i></a></li>
                   <li><a href="https://www.instagram.com/safirisalama/"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                   <li><a href="https://www.youtube.com/channel/UCz3izliYASJpfIlXxN0VJBQ"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
               </ul>
           </div>
       </div>
   </div>
</footer>