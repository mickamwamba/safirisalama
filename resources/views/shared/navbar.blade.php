<nav class="navbar navbar-custom navbar-static-top {{$navbarClass}}">
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle visible-xs drawer-btn" data-toggle="offcanvas" data-recalc="false" data-target=".navmenu" data-canvas=".canvas">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
       <a class="navbar-brand" href="/">
        <img alt="Brand" class="navbar-brand-img" src="{{url('/images/Logo/brand.png')}}">
      </a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
    
      <ul class="nav navbar-nav navbar-right">
       <li class="active"><a href="/allNews">Habari</a></li>
        <li ><a href="/asasi">Asasi za kiraia</a></li>
        <li ><a href="/sheria">Mapendekezo ya sheria</a></li>
        <li><a href="/stats">Takwimu</a></li>
        <li><a href="/reports">Ripoti</a></li>
        
        
        <a href="/take_action">
          <div class="btn btn-default navbar-btn {{$btnClass}}">
            <span class="btn-title">CHUKUA HATUA</span>
          </div>
        </a>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>