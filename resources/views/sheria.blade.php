@extends ('layouts.client')
@section('title','Mapendekezo ya sheria')
@section('content')
@component('shared.navbar')
@slot('navbarClass')
    navbar-safiri
@endslot
@slot('btnClass')
    action-btn-inverse
@endslot
@slot('buttonTheme')
    button-theme-inverse
@endslot
@endcomponent
    <div class="stats-main-block">
        <div class="container">
            <div class="row">
                <h1 class="center-takwimu-header">Mapendekezo ya kurekebisha sheria</h1>
                <div class="col-xs-8 col-xs-offset-2">
                                    <h4 class="center-page-header"></h4>

                </div>
            </div>
        </div>
    </div>
    <div class="sheria-main-block">
        <div class="container">
        <div class="row">
            <div class="col-md-6 pendekezo-thumbnail">
            <h4>Pendekezo #1</h4>
            <img class="img-responsive" src="./images/cartoons/cartoon2.jpg" alt="">
            </div>
               <div class="col-md-6 pendekezo-thumbnail">
            <h4>Pendekezo #2</h4>
            <img class="img-responsive" src="./images/cartoons/cartoon1.jpg" alt="">
            </div>
       <div class="col-md-6 pendekezo-thumbnail">
            <h4>Pendekezo #3</h4>
            <img class="img-responsive" src="./images/cartoons/cartoon3.jpg" alt="">
            </div>
       <div class="col-md-6 pendekezo-thumbnail">
            <h4>Pendekezo #4</h4>
            <img class="img-responsive" src="./images/cartoons/cartoon4.jpg" alt="">
            </div>
               <div class="col-md-6 col-md-offset-3 pendekezo-thumbnail">
            <h4>Pendekezo #5</h4>
            <img class="img-responsive" src="./images/cartoons/cartoon5.jpg" alt="">
            </div>
        </div>
        </div>
    </div>

    @component('shared.footer')
@endcomponent
@endsection
