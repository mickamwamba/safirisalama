@extends ('layouts.client')
@section('title','Takwimu')
@section('content')
@component('shared.navbar')
@slot('navbarClass')
    navbar-safiri
@endslot
@slot('btnClass')
    action-btn-inverse
@endslot
@slot('buttonTheme')
    button-theme-inverse
@endslot
@endcomponent
{{-- @include('includes.chartjs') --}}
    <div class="stats-main-block">
        <div class="container">
            <div class="row">
                <h1 class="center-takwimu-header">Takwimu</h1>
                <div class="col-xs-8 col-xs-offset-2">
                                    <h4 class="center-page-header">Taarifa kutoka jeshi la Polisi la Tanzania</h4>

                </div>
            </div>
        </div>
    </div>
    {{--  All the charts shall reside here  --}}
    <div class="bar-chart-block">
            <div class="container">
                  <h1 class="center-header-percentage">Takwimu za ajali 2014-2017</h1>

                <div class="row">
                    <div class="col-sm-12 bar-chart-container" >
                        <canvas id="myChart"></canvas>
                    </div>
                </div>
            </div>
    </div>
    <div class="percentage-chart-block">
        <div class="container">
            <div class="row percentage-row ">
                <h1 class="center-header-percentage">Vyanzo vya ajali barabarani</h1>
                <div class="col-sm-4 circle-graph-wrapper">
                         <div class="c100 p85 circle-graph">
                    <span>86.6%</span>
                    <div class="slice">
                        <div class="bar"></div>
                        <div class="fill"></div>
                    </div>
                </div>
                <h4>Makosa ya kibinaadamu</h4>
                </div>
                <div class="col-sm-4 circle-graph-wrapper">
                         <div class="c100 p10 circle-graph">
                    <span>7.7%</span>
                    <div class="slice">
                        <div class="bar"></div>
                        <div class="fill"></div>
                    </div>
                </div>
                <h4>Ubovu wa vyombo vya moto</h4>
                </div>
                <div class="col-sm-4 circle-graph-wrapper ">
                         <div class="c100 p5 circle-graph">
                    <span>5.9%</span>
                    <div class="slice">
                        <div class="bar"></div>
                        <div class="fill"></div>
                    </div>
                </div>
                <h4>Ubovu wa barabara</h4>
                </div>
            </div>
        </div>
    </div>
@component('shared.footer')
@endcomponent
@endsection
