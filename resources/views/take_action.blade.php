@extends('layouts.client')
@section('content')
@component('shared.navbar')
@slot('navbarClass')
    navbar-solid
@endslot
@slot('btnClass')
    action-btn
@endslot
@slot('buttonTheme')
    button-theme
@endslot
@endcomponent

<div class="main-wrapper">
  <div class="container">

        @if (Session::has('msg'))
        <div class="alert alert-info" style="text-align:center">{{ Session::get('msg') }}</div>
     @endif
        <div class="row">
                <div class="col-md-8 col-md-offset-2" style="text-align:center">
                    <div class="text-wrapper">
                         <h2>Mimi <span style="color:#d52147">nachukua hatua</span></h2>
                        <h3>#Ajalisasabasi</h3>
                        <p>Andika jina lako, kisha sambaza ujumbe wenye jina lako kuhusu usalama barabarani kwenye mitandao ya kijamii</p>
        
                    </div>
                </div>
         </div>
  </div>

  <div class="form-wrapper">
        <form method="GET" action="/shareImage">
            <div class="form-row">
                <div class="col-md-4 col-md-offset-2">
                <input type="text" class="form-control" name="name" placeholder="Name" required>
                </div>
                <div class="col-md-4">
                <input type="text" class="form-control" name ="email" placeholder="name@example.com">
                </div> 
                {{csrf_field()}}
                <div class="sub-btn row">
                    <div class="col-md-4 col-md-offset-4">
                            <br><br><input type="submit" value="CHUKUA HATUA" class="form-control" placeholder="">
                    </div>
                </div>
            </div>
            
        </form>
    </div> 

</div>
 
 @component('shared.footer')
 @endcomponent

 @endsection