<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::prefix('admin')->group(function(){
Route::domain('admin.localhost')->group(function () {
	// Route::domain('admin.safirisalama.org')->group(function () {

	Route::view('login', 'auth.login_admin')->name('admin.login');
	Route::post('login', 'Admin\AdminController@authenticateAdmin')->name('admin.loginpost');

	Route::middleware(['auth_admin'])->group(function () {
		Route::get('logout', 'Admin\AdminController@logout');
		Route::get('/', 'Admin\NewsController@getNews')->name('admin.dashboard');
		Route::get('news', 'Admin\NewsController@getNews')->name('admin.allNews');
		Route::get('addNews', 'Admin\NewsController@showAddPanel');
		Route::post('addNews', 'Admin\NewsController@addNews')->name('admin.addNews');
		Route::get('editNews/{id}', 'Admin\NewsController@showEditPanel');
		Route::post('editNews', 'Admin\NewsController@editNews')->name('admin.editNews');
		Route::get('deleteNews/{id}', 'Admin\NewsController@deleteNews')->name('admin.deleteNews');

		Route::get('asasi', 'Admin\AsasiController@getAsasi')->name('admin.asasi');
		Route::get('addAsasi', 'Admin\AsasiController@showAddAsasi');
		Route::post('addAsasi', 'Admin\AsasiController@AddAsasi')->name('admin.addAsasi');
		Route::get('edit_asasi/{asasi_id}', 'Admin\AsasiController@showEditAsasiPanel');
		Route::post('editAsasi', 'Admin\AsasiController@editAsasi')->name('admin.editAsasi');
		Route::get('delete_asasi/{asasi_id}', 'Admin\AsasiController@deleteAsasi');

		Route::get('events', 'Admin\EventController@getEvents');
		Route::get('addEvent', 'Admin\EventController@showAddPanel');
		Route::post('addEvent', 'Admin\EventController@addEvent');
		Route::get('editEvent/{id}', 'Admin\EventController@showEditPanel');
		Route::post('editEvent', 'Admin\EventController@editEvent');
		Route::get('deleteEvent/{id}', 'Admin\EventController@deleteEvent');

		Route::get('ripoti', 'Admin\ResourceController@getResources')->name('admin.resources');
		Route::post('addRipoti', 'Admin\ResourceController@addRipoti')->name('admin.addRipoti');
		Route::get('editResource/{resource_id}', 'Admin\ResourceController@showEditRipotiPanel');
		Route::post('editRipoti', 'Admin\ResourceController@editRipoti')->name('admin.editRipoti');
		Route::get('delete_resource/{resource_id}', 'Admin\ResourceController@deleteResource');


		Route::get('eventMedia/{event_id}', 'Admin\EventMediaController@getMedia');
		Route::post('addMedia', 'Admin\EventMediaController@addMedia');
		Route::get('editEventMedia/{event_media_id}', 'Admin\EventMediaController@showEditPanel');
		Route::post('editEventMedia', 'Admin\EventMediaController@editEventMedia');
		Route::get('deleteEventMedia/{id}', 'Admin\EventMediaController@deleteEventMedia');

		Route::get('advocates', 'Admin\AdvocateController@getAdvocates');
		Route::post('verifyAdvocate', 'Admin\AdvocateController@verifyAdvocate');

		Route::get('pdfview', array('as' => 'pdfview', 'uses' => 'Admin\AdvocateController@pdfview'));


		Route::post('updateAdmin', 'Admin\AdminController@updateAdmin')->name('admin.update_details');
		Route::post('updatePassword', 'Admin\AdminController@updatePassword')->name('admin.update_password');
	});
});

Route::get('/', 'IndexController@getIndex');

Route::get('take_action', function () {
	return view('take_action');
});

Route::get('asasi', function () {
	return view('asasi');
});

Route::get('sheria', function () {
	return view('sheria');
});

Route::get('allNews', 'NewsController@getAllNews');

Route::get('news/{id}', 'NewsController@getNews');

Route::get('events', 'EventController@getEvents');

Route::get('shareImage', 'ImageController@createImage');
Route::get('share_image', function () {
	return view('share_image');
});

Route::post('getToken', 'ImageController@shareToFB');
Route::get('stats', function () {
	return view('stats');
});

Route::get('event/{id}', 'EventController@getEvent');

Route::get('reports', 'ResourceController@getResources');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
